##### Table of contents

* Overview
* How to run
* A note of the binary-product algebra slices
* License
* References

# Overview

_This version: 18 Aug 2022_

Program to search
for [N-ary](https://en.wikipedia.org/wiki/Arity) [algebras](https://en.m.wikipedia.org/wiki/Algebra_over_a_field)
in a finite `D`-dimensional vector space over the reals `R`, that are equipped with some
[form](https://en.wikipedia.org/wiki/Multilinear_form) `|.|` that is preserved under product composition:

```text
|.| : R^D -> R,

      |a||b| = | *(a,b) |     for all a,b in R^D
                                under a binary multiplication (conventional multiplication)
                                * : R^D (X) R^D --> R^D,
                                 
   |a||b||c| = | *(a,b,c) |   for all a,b,c in R^D
                                under a ternary multiplication
                                * : R^D (X) R^D (X) R^D --> R^D,
                                
|a||b||c||d| = | *(a,b,c,d) | for all a,b,c,d in R^D
                                under a quaternary multiplication
                                * : R^D (X) R^D (X) R^D (X) R^D --> R^D,
            
            ...
```

and so on.

Real numbers are approximated by 64-bit floating point precision variables in Java. The algorithm first uses a custom
local search algorithm to find [structure constants](https://en.wikipedia.org/wiki/Structure_constants) for algebras,
with algebra basis elements at arbitrary orientation in the vector space. It then attempts to "simplify" the structure
constants by axis-aligning the algebra basis elements with the vector space basis, such that the result looks more like
a conventional multiplication table.

Examples for algebras with such a preserved form under product composition are the
[normed division algebras](https://en.wikipedia.org/wiki/Normed_division_algebra)
(real numbers, [complex numbers](https://en.wikipedia.org/wiki/Complex_number)
, [quaternions](https://en.wikipedia.org/wiki/Quaternion), and [octonions](https://en.wikipedia.org/wiki/Octonion)). For
these, the norm `|a|` is simply the Euclidean norm, i.e., the length of the vector in `R^D` that represents each number.
[Composition algebras](https://en.wikipedia.org/wiki/Composition_algebra)
generalize the normed division algebras by including counterparts that preserve some split-"norm". Because the code here
doesn't require a multiplicative unit element, and doesn't require the composition form to be quadratic and
nondegenerate, it allows finding further algebras such as
[Okubo octonions](https://en.wikipedia.org/wiki/Okubo_algebra) or ternary split-quaternions.

Note that human-friendly simplification of the structure constants currently only attempts to align the algebra basis
elements with the orthogonal basis of the vector space, however, will not (yet) go further and attempt to find whether
the algebra is unital, i.e., whether a unit element exists (or not). You may consider algebraic computer system, such as
[GAP](https://www.gap-system.org/) with the [LOOPS](https://www.gap-system.org/Packages/loops.html) package, to help
further analyze the structure of a found algebra.

# How to run

See [`src/main/java/nary/NaryCompositionFinderApp.java`](src/main/java/nary/NaryCompositionFinderApp.java) for in-code
documentation.

After checking out the code from the repo, the program is configured to find 4-ary "quaternions" in 4 dimensions. Change
the following lines of code to search for algebras with different arity, dimension, and preserved composition forms:

```text
public final static int DIMENSION = 4;
public final static int ARITY = 4;
public final static Composition COMPOSITION = new CompEuclidean();
```

# A note of the binary-product algebra slices

Once an algebra is found and the structure constants axis-aligned, the code looks for binary-product slices within the
structure constants that preserve the configured `COMPOSITION`. This is in file `NaryCompositionFinderApp`
method `testPairwiseComposition`. This flaw needs to be fixed in a future version.

This flaw led me to a false hypothesis [here (July 2022)](https://groups.io/g/hypercomplex/message/181), which I've
since corrected [here (August 2022)](https://groups.io/g/hypercomplex/message/197).

# License

License CC BY-SA 4.0 (2022) Jens Koeplinger (Creative Commons Share-alike with Attribution v4.0)

# References

* [Arity](https://en.wikipedia.org/wiki/Arity)
* [Complex numbers](https://en.wikipedia.org/wiki/Complex_number)
* [Composition algebra](https://en.wikipedia.org/wiki/Composition_algebra)
* [Form (algebra)](https://en.wikipedia.org/wiki/Multilinear_form)
* [GAP](https://www.gap-system.org/) Groups, Algorithms, Programming (system for computational discrete algebra)
    * [LOOPS](https://www.gap-system.org/Packages/loops.html) package for quasigroups and loops, like the (split-)
      octonion product
* [Normed division algebra](https://en.wikipedia.org/wiki/Normed_division_algebra)
    * [John Baez "The Octonions" (seminal 2001 review paper)](https://arxiv.org/abs/math/0105155)
* [Octonions](https://en.wikipedia.org/wiki/Octonion)
* [Okubo octonions](https://en.wikipedia.org/wiki/Okubo_algebra), an algebra without unit element, but with
  product-composition property of a Euclidean 2-form
    * ["Okubo Quasigroups" (2022 paper by Jonathan D. H. Smith and Petr Vojtěchovský)](https://www.elibm.org/ft/10012190000)
* [Quaternions](https://en.wikipedia.org/wiki/Quaternion)
    * quaternionic triality (in ternary quaternions) example references:
      [John Baez' "this week's finds" week 91](https://math.ucr.edu/home/baez/week91.html),
      ["Division algebraic symmetry breaking" (2022 paper by Nichol Furey and Mia Hughes)](https://doi.org/10.1016/j.physletb.2022.137186)
      ,
      [video presentation (e.g. from minute 41)](https://www.youtube.com/watch?v=BvHqAGg2CME&t=3s)
* [Split-complex numbers](https://en.wikipedia.org/wiki/Split-complex_number)
* [Split-octonions](https://en.wikipedia.org/wiki/Split-octonion)
* [Split-quaternions](https://en.wikipedia.org/wiki/Split-quaternion)
* [Structure constants](https://en.wikipedia.org/wiki/Structure_constants)

