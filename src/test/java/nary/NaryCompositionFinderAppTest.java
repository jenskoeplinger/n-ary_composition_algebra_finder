/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary;

import nary.composition.Composition;
import nary.composition.CompositionLocalsearchSolver;
import nary.composition.impl.*;
import nary.helper.MatrixMath;
import nary.helper.NaryMath;
import nary.model.NaryStrucons;
import nary.model.Vector;
import nary.simplify.Entropy;
import nary.simplify.NeighborhoodSearch;
import nary.simplify.impl.EntropyAxisAligned;
import nary.simplify.impl.EntropySortedPairWeightedDistance;
import nary.simplify.impl.NeighborhoodSearchEntriesSingleRotation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * A bunch of unit tests to qualify the algebra, solver, etc.
 */
public class NaryCompositionFinderAppTest {

    private static Logger logger = LoggerFactory.getLogger(NaryCompositionFinderAppTest.class);

    static {
        Vector.setRandom(new Random(new Date().getTime()));
    }

    @Before
    public void beforeAll() {
        logger.info("---------- test:");
    }

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void testMultiplicationStrucons() {

        Composition metricComposition = new CompEuclidean();

        // test in 2, 3, and 4 dimensions and arities: Seed new N-ary structure constants with random entries

        for (int dimens = 2; dimens <= 4; dimens++) {
            for (int arity = 2; arity <= 4; arity++) {
                logger.info("dimension " + dimens + ", arity " + arity);
                NaryStrucons strucons = new NaryStrucons(metricComposition, dimens, arity);
                strucons.normalize(1.);
                logger.info("  created new N-ary structure constants:\n\n" + strucons);

                logger.info("  check dimensionality and number of entries");
                Assert.assertEquals(strucons.getEntries().length, NaryStrucons.calculateStruconsSize(dimens, arity));

                logger.info("  check proper norm of all entries");
                for (int entryIdx = 0; entryIdx < strucons.getEntries().length; entryIdx++) {
                    assertEquals(strucons.getEntryAt(entryIdx).getLength(), 1., 1.e-10);
                }
            }
        }

    }

    @Test
    public void testComplexNumbers() {

        // test in 2 dimensions: complex numbers

        Composition metricComposition = new CompEuclidean();

        Vector one = new Vector(metricComposition, 1., 0);
        Vector minusOne = new Vector(one).negate();
        Vector i = new Vector(metricComposition, 0., 1);
        Vector minusI = new Vector(i).negate();
        Vector q1 = new Vector(metricComposition, 1., 1.).setLength(1.); // should become ( 1/sqrt(2) , 1/sqrt(2) )
        Vector q2 = new Vector(metricComposition, -1., 1.).setLength(1.);
        Vector q3 = new Vector(metricComposition, -1., -1.).setLength(1.);
        Vector q4 = new Vector(metricComposition, 1., -1.).setLength(1.);

        NaryStrucons complexStrucons = new NaryStrucons(metricComposition, 2, 2); // 2x2 multiplication table

        complexStrucons.setEntryAt(new Vector(one), 0, 0);
        complexStrucons.setEntryAt(new Vector(i), 1, 0);
        complexStrucons.setEntryAt(new Vector(i), 0, 1);
        complexStrucons.setEntryAt(new Vector(one).negate(), 1, 1);

        logger.info("Complex number multiplication tests");
        logger.info("Structure constants:\n\n" + complexStrucons);

        assertEquals(0, testMultDiff(complexStrucons, one, one, one), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, i, one, i), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, i, i, one), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, minusOne, i, i), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, one, q1, q4), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, one, q2, q3), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, i, q1, q1), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, minusI, q2, q2), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, i, q3, q3), 1.e-10);
        assertEquals(0, testMultDiff(complexStrucons, minusI, q4, q4), 1.e-10);

    }

    private double testMultDiff(NaryStrucons strucons, Vector expected, Vector... factors) {
        Vector actual = strucons.multiply(factors);
        logger.info("  product of " + Arrays.toString(factors) + " = " + actual + " (should be " + expected + ")");
        return actual.subtract(expected).getLength();
    }

    @Test
    public void testQuaternions() {

        // test in 4 dimensions: quaternions

        Composition metricComposition = new CompEuclidean();

        Vector one = new Vector(metricComposition, 1., 0, 0, 0);
        Vector minusOne = new Vector(one).negate();
        Vector i1 = new Vector(metricComposition, 0, 1., 0, 0);
        Vector minusI1 = new Vector(i1).negate();
        Vector i2 = new Vector(metricComposition, 0, 0, 1., 0);
        Vector minusI2 = new Vector(i2).negate();
        Vector i3 = new Vector(metricComposition, 0, 0, 0, 1.);
        Vector minusI3 = new Vector(i3).negate();

        NaryStrucons quaternionStrucons = new NaryStrucons(metricComposition, 4, 4); // 4x4x4x4 multiplication table

        // set the first 4x4 plane manually using quaternion multiplication
        quaternionStrucons.setEntryAt(one, 0, 0, 0, 0);
        quaternionStrucons.setEntryAt(i1, 1, 0, 0, 0);
        quaternionStrucons.setEntryAt(i2, 2, 0, 0, 0);
        quaternionStrucons.setEntryAt(i3, 3, 0, 0, 0);
        quaternionStrucons.setEntryAt(i1, 0, 1, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 1, 1, 0, 0);
        quaternionStrucons.setEntryAt(minusI3, 2, 1, 0, 0);
        quaternionStrucons.setEntryAt(i2, 3, 1, 0, 0);
        quaternionStrucons.setEntryAt(i2, 0, 2, 0, 0);
        quaternionStrucons.setEntryAt(i3, 1, 2, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 2, 2, 0, 0);
        quaternionStrucons.setEntryAt(minusI1, 3, 2, 0, 0);
        quaternionStrucons.setEntryAt(i3, 0, 3, 0, 0);
        quaternionStrucons.setEntryAt(minusI2, 1, 3, 0, 0);
        quaternionStrucons.setEntryAt(i1, 2, 3, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 3, 3, 0, 0);

        // test quaternions so far (effectively only using the first two numbers, leaving the 3rd and 4th as "one")
        assertEquals(0, testMultDiff(quaternionStrucons, one, one, one, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i1, one, i1, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i1, i1, one, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusOne, i1, i1, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i2, one, i2, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i2, i2, one, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusOne, i2, i2, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i3, one, i3, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i3, i3, one, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusOne, i3, i3, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i3, i1, i2, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusI3, i2, i1, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i2, i3, i1, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusI2, i1, i3, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, i1, i2, i3, one, one), 1.e-10);
        assertEquals(0, testMultDiff(quaternionStrucons, minusI1, i3, i2, one, one), 1.e-10);

        // at this point the 16 quaternion entries in the 4x4 table are validated
        // -> auto-calculate the third and fourth dimensions by using quaternion rules

        logger.info("auto-generate structure constants for 4x4x4x4 quaternary quaternion multiplication hypercube");

        Vector entry;
        Vector prod;

        for (int factorIdx0 = 0; factorIdx0 < 4; factorIdx0++) {
            logger.info("   factorIdx0=" + factorIdx0);
            for (int factorIdx1 = 0; factorIdx1 < 4; factorIdx1++) {
                logger.info("      factorIdx1=" + factorIdx1);

                entry = quaternionStrucons.getEntryAt(factorIdx0, factorIdx1, 0, 0);

                prod = quaternionStrucons.multiply(entry, i1, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 1, 0);
                prod = quaternionStrucons.multiply(entry, i2, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 2, 0);
                prod = quaternionStrucons.multiply(entry, i3, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 3, 0);

                for (int factorIdx2 = 0; factorIdx2 < 4; factorIdx2++) {

                    entry = quaternionStrucons.getEntryAt(factorIdx0, factorIdx1, factorIdx2, 0);

                    prod = quaternionStrucons.multiply(entry, i1, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 1);
                    prod = quaternionStrucons.multiply(entry, i2, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 2);
                    prod = quaternionStrucons.multiply(entry, i3, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 3);

                }
            }
        }

        // Test composition property of the general quaternion product
        for (int test = 0; test < 1000; test++) {

            // pick four numbers, in arbitrary direction, with lengths in [0., 10.[
            Vector h0 = new Vector(metricComposition, 4).setLength(Math.random() * 10.);
            Vector h1 = new Vector(metricComposition, 4).setLength(Math.random() * 10.);
            Vector h2 = new Vector(metricComposition, 4).setLength(Math.random() * 10.);
            Vector h3 = new Vector(metricComposition, 4).setLength(Math.random() * 10.);

            if (test < 10) {
                logger.info("Test composition property of the quaternary quaternion product:\n"
                        + h0 + " *\n" + h1 + " *\n" + h2 + " *\n" + h3);
            } else if (test == 10) {
                logger.info("  (omitting detail output for the remaining tests)");
            }

            Vector product = quaternionStrucons.multiply(h0, h1, h2, h3);
            if (test < 10) logger.info("product = " + product);

            double l0 = h0.getLength();
            double l1 = h1.getLength();
            double l2 = h2.getLength();
            double l3 = h3.getLength();
            double composition = l0 * l1 * l2 * l3;

            if (test < 10) logger.info("composition = " + composition);

            double lProduct = product.getLength();

            if (test < 10) logger.info("length of product = " + lProduct);

            assertEquals(composition, lProduct, 1.e-8);
        }

    }

    @Test
    public void testCompositionFunctions() {

        logger.info("testing various composition functions");

        Composition metricComposition = new CompEuclidean();

        CompEuclidean compEuclidean = new CompEuclidean();
        CompMinkowskian compMinkowskian = new CompMinkowskian();
        CompEuclideanMinkowskian compEuclideanMinkowskian = new CompEuclideanMinkowskian();
        CompPowerThree compPowerThree = new CompPowerThree();
        CompPowerFour compPowerFour = new CompPowerFour();
        CompPowerDimens compPowerDimens = new CompPowerDimens();
        CompTaxi compTaxi = new CompTaxi();
        CompSplitTaxi compSplitTaxi = new CompSplitTaxi();
        CompMixedTaxi compMixedTaxi = new CompMixedTaxi();

        Vector testNumber;
        double composition;
        double expected;

        testNumber = new Vector(metricComposition, 1., -2., 3);
        composition = compEuclidean.calculate(testNumber);
        expected = 1. + 4. + 9.;
        assertEquals(expected, composition, 1.e-10);

        composition = compMinkowskian.calculate(testNumber);
        expected = 1. - 4. - 9.;
        assertEquals(expected, composition, 1.e-10);

        composition = compEuclideanMinkowskian.calculate(testNumber);
        expected = (1. + 4. + 9.) * (1. - 4. - 9.);
        assertEquals(expected, composition, 1.e-10);

        composition = compPowerThree.calculate(testNumber);
        expected = 1. - 8. + 27.;
        assertEquals(expected, composition, 1.e-10);

        composition = compPowerFour.calculate(testNumber);
        expected = 1. + 16. + 81.;
        assertEquals(expected, composition, 1.e-10);

        composition = compPowerDimens.calculate(testNumber);
        expected = 1. - 8. + 27.;
        assertEquals(expected, composition, 1.e-10);

        composition = compTaxi.calculate(testNumber);
        expected = 1. + 2. + 3.;
        assertEquals(expected, composition, 1.e-10);

        composition = compSplitTaxi.calculate(testNumber);
        expected = 1. - 2. - 3.;
        assertEquals(expected, composition, 1.e-10);

        composition = compMixedTaxi.calculate(testNumber);
        expected = (1. + 2. + 3.) * (1. - 2. - 3.);
        assertEquals(expected, composition, 1.e-10);

    }

    @Test
    public void testSolverOnQuaternionProduct() {

        logger.info("test solver starting out with a perfectly axis-aligned product of four quaternions");

        NaryStrucons strucons = NaryMath.createQuaternaryQuaternionStrucons();

        CompositionLocalsearchSolver solver = new CompositionLocalsearchSolver(
                4,
                4,
                new CompEuclidean(),
                new CompEuclidean(),
                strucons,
                0.1,
                .01,
                1.e-15,
                1000L
        );
        solver.solve();

        // As of v1.0 this should take well fewer than 10000 iterations to finish (if this throws
        // an error, check local-search scale and abort precision against algorithm changes since v1.0)
        assertTrue(solver.getIterationCnt() < 10000L);

        NaryStrucons testStrucons = NaryMath.createQuaternaryQuaternionStrucons();

        for (int entryIdx = 0; entryIdx < 256; entryIdx++) {
            // assert strict equals; since all entries must be 0, 1., or -1., strict equals() on double needs to pass
            assertEquals(strucons.getEntryAt(entryIdx), testStrucons.getEntryAt(entryIdx));
        }

    }

    @Test
    public void testSolverOnAlmostQuaternionProduct() {

        NaryStrucons strucons = NaryMath.createQuaternaryQuaternionStrucons();
        Composition metricComposition = new CompEuclidean();

        logger.info("Starting with slightly faulty structure constants of product of four quaternions.");

        double imperfection = 0.1;

        logger.info("To each entry in the perfectly axis-aligned structure constants (multiplication table), " +
                "add a vector of length " + imperfection + " and random direction.");

        // deviate each entry of the ideal quaternionic quaternion structure constants by an imperfection in random direction
        for (int entryIdx = 0; entryIdx < 256; entryIdx++) {
            strucons.getEntryAt(entryIdx).add(new Vector(metricComposition, 4).setLength(imperfection));
        }

        logger.info("The new, noisy structure constants are:\n\n" + strucons);

        logger.info("Now see whether it finds its way back.");

        CompositionLocalsearchSolver solver = new CompositionLocalsearchSolver(
                4,
                4,
                new CompEuclidean(),
                new CompEuclidean(),
                strucons,
                0.1,
                .01,
                1.e-15,
                1000L
        );
        solver.solve();

        // This may take quite a few iterations to finish. The result won't be the exact quaternion structure constants,
        // however, since we started so close to perfect it should be very nearby.

        NaryStrucons testStrucons = NaryMath.createQuaternaryQuaternionStrucons();

        for (int entryIdx = 0; entryIdx < 256; entryIdx++) {
            // Assert equals with a precision of the initial imperfection.
            // In most tests this should be more than enough to pass.
            // In rare cases, this may violate the test. Re-run the test to confirm.
            for (int axis = 0; axis < 4; axis++) {
                double struconsCoef = strucons.getEntryAt(entryIdx).getCoefAt(axis);
                double testStruconsCoef = testStrucons.getEntryAt(entryIdx).getCoefAt(axis);
                assertEquals(struconsCoef * struconsCoef, testStruconsCoef * testStruconsCoef, imperfection);
            }
        }

    }

    @Test
    public void testEntropyAxisAligned() {

        logger.info("Test axis aligned entropy with perfectly axis-aligned quaternion" +
                " structure constants (multiplication table)");

        Composition metricComposition = new CompEuclidean();
        NaryStrucons strucons = NaryMath.createQuaternaryQuaternionStrucons();
        Entropy entropy = new EntropyAxisAligned();
        double result = entropy.calculate(strucons);
        logger.info("Result: " + result);

        assertEquals(0, result, 1.e-10);

        logger.info("Test structure constants representing a 3x3x3 multiplication table" +
                " that has maximum entropy (all entries are 0.5)");
        strucons = new NaryStrucons(metricComposition, 3, 3);
        for (Vector entry : strucons.getEntries()) {
            entry.setCoefAt(0.5, 0);
            entry.setCoefAt(0.5, 1);
            entry.setCoefAt(-0.5, 2);
        }
        result = entropy.calculate(strucons);
        logger.info("Result: " + result);

        assertEquals(1., result, 1.e-10);

        logger.info("Tests a 5x5x5x5x5 table with random entries on the unit sphere");
        strucons = new NaryStrucons(metricComposition, 5, 5).normalize(1.);
        result = entropy.calculate(strucons);
        double expectedAverage = 0.53;
        logger.info("Result: " + result + " (expected: around " + expectedAverage + ")");

        logger.info("This test may fail in rare occasions due to the randomness. Re-run first before assuming a bug.");

        assertEquals(expectedAverage, result, 0.05);

    }

    @Test
    public void testEntropySortedPairWeightedDistance() {

        logger.info("Test sorted pair weighted distance entropy with perfectly axis-aligned" +
                " quaternion structure constants (entropy 0)");

        Composition metricComposition = new CompEuclidean();
        NaryStrucons strucons = NaryMath.createQuaternaryQuaternionStrucons();
        Entropy entropy = new EntropySortedPairWeightedDistance();
        double result = entropy.calculate(strucons);
        logger.info("Result: " + result);

        assertEquals(0., result, 1.e-10);

        logger.info("Test structure constants representing a 3x3x3 multiplication table that has entropy 1." +
                " (all entries are +/-0.5)");
        strucons = new NaryStrucons(metricComposition, 3, 3);
        for (Vector entry : strucons.getEntries()) {
            entry.setCoefAt(0.5, 0);
            entry.setCoefAt(0.5, 1);
            entry.setCoefAt(-0.5, 2);
        }
        result = entropy.calculate(strucons);
        logger.info("Result: " + result);

        assertEquals(1., result, 1.e-10);

        logger.info("Tests a 5x5x5x5x5 table with random entries on the unit sphere");
        strucons = new NaryStrucons(metricComposition, 5, 5).normalize(1.);
        result = entropy.calculate(strucons);
        double expected = 1. - 1. / ((double) (strucons.getEntries().length));
        logger.info("Result: " + result + " (expected: around " + expected + ")");

        logger.info("This test may fail in rare occasions due to the randomness. Re-run first before assuming a bug.");

        assertEquals(expected, result, (1. - expected) * Math.sqrt(expected));

    }

    @Test
    public void testNeighborhoodSearchSingleAxis() {

        logger.info("Test NeighborhoodSearchEntriesSingleRotation by starting with structure constants:");
        logger.info("- calculate its residual from Euclidean norm");
        logger.info("- performing a neighborhood search with maximum randomness");
        logger.info("- calculate its residual again; it should not have changed");

        Composition compositionImpl = new CompEuclidean();
        Composition metricComposition = new CompEuclidean();
        NeighborhoodSearch neighborhoodSearchImpl = new NeighborhoodSearchEntriesSingleRotation();
        neighborhoodSearchImpl.setRandom(new Random());
        double averageDifference = 0;
        int numTests = 20;
        double rotateStrength = 1.;

        for (int test = 0; test < numTests; test++) {

            NaryStrucons strucons = new NaryStrucons(metricComposition, 5, 5).normalize(1.);
            Vector.setRandom(new Random(test));
            double residualBefore = NaryMath.calculateRandomResiduals(strucons, compositionImpl, 300);
            neighborhoodSearchImpl.step(strucons, rotateStrength);
            Vector.setRandom(new Random(test));
            double residualAfter = NaryMath.calculateRandomResiduals(strucons, compositionImpl, 300);

            if (test < 10) {
                logger.info("test #" + test
                        + " (N-ary structure constants in N dimensions for N=" + strucons.getDimens()
                        + ") residual before: " + residualBefore
                        + ", residual after: " + residualAfter
                        + ", difference: " + Math.abs(residualAfter - residualBefore));
            } else if (test == 10) {
                logger.info("  (suppressing output for the remaining tests)");
            }

            averageDifference += Math.abs(residualAfter - residualBefore);
        }

        averageDifference = averageDifference / ((double) numTests);

        logger.info("Average residuals absolute difference: " + averageDifference);

        assertEquals(0., averageDifference, 1.e-15);

    }

    @Test
    public void testStruconsTransformations() {
        logger.info("Test NaryMath.transformStrucons(...) by:");
        logger.info("- starting out with quaternions,");
        logger.info("- apply a 2D subspace rotation along two random axes,");
        logger.info("- confirm that it still holds the composition rule");

        Composition compositionImpl = new CompEuclidean();
        NaryStrucons strucons = NaryMath.createQuaternaryQuaternionStrucons();
        double testResidual = NaryMath.calculateRandomResiduals(strucons, compositionImpl, 300);
        logger.info("testResidual from 300 random tests (should be 0): " + testResidual);

        int numTests = 20;
        int dimens = 4;
        int arity = 4;
        Random random = new Random();
        double[][] rotationMatrix = MatrixMath.createRandomSO2Rotation(random, dimens, 1.);

        for (int test = 0; test < numTests; test++) {

            // apply another random rotation to the matrix so that it'll get more and more convoluted
            rotationMatrix = MatrixMath.matrixProduct(
                    rotationMatrix, MatrixMath.createRandomSO2Rotation(random, dimens, 1.), dimens);

            if (test < 3) {
                // the three rotations should be in the subspace with axes 0 and 1 only
                NaryMath.transformStrucons(strucons, rotationMatrix, 0, 1);
            } else {
                int factorIdx1 = (int) (random.nextDouble() * ((double) arity));
                int factorIdx2 = (int) (random.nextDouble() * ((double) (arity - 1)));
                if (factorIdx2 == factorIdx1) factorIdx2 = arity - 1;
                NaryMath.transformStrucons(strucons, rotationMatrix, factorIdx1, factorIdx2);
            }

            testResidual = NaryMath.calculateRandomResiduals(strucons, compositionImpl, 300);

            if (test < 10) {
                logger.info("applied rotation matrix: " + Arrays.deepToString(rotationMatrix));
                logger.info("resulting structure constants:\n" + strucons);
                logger.info("residuals of composition property after 300 random tests: " + testResidual);

            } else if (test == 10) {
                logger.info("  (suppressing output for the remaining tests)");
            }

            assertEquals(0, testResidual, 1.e-10);
        }

    }

}
