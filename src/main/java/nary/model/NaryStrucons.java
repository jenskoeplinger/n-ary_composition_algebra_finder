/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.model;

import nary.composition.Composition;

import java.util.Arrays;

/**
 * Structure constants representing an N-ary multiplication table (hypercube) of size N ("arity") and dimension
 * D ("dimens"), i.e., with * D^N table entries. Each entry is again an D-dimensional vector, i.e., not necessarily
 * axis-aligned as is convention for multiplication-table representation of common number systems
 * (quaternions, octonions, etc).
 * <p>
 * Methods return the object instance (where reasonable) so that it can be invoked as builder.
 * <p>
 * NOTE: Each structure constants entry _must_ point to a distinct instance of Vector. It is not valid to
 * have two or more entries to the same instance of Vector, since number manipulation (add, subtract,
 * set length, set random) are done in-place (in-memory).
 */
public class NaryStrucons {

    /**
     * Dimensionality of the vector space.
     */
    private final int dimens;

    /**
     * Arity (rank) of the vector multiplication (product) represented by these structure constants
     */
    private final int arity;

    /**
     * Total number of entries (convenience variable)
     */
    private final int numEntries;

    /**
     * Function to use for calculating metric values (e.g. unit distance to the origin)
     */
    private final Composition metricComposition;

    /**
     * Structure constants, represented as vectors in a generalized multiplication table in N dimensions.
     * Entries must not be null.
     */
    private final Vector[] entries;

    // ----------


    /**
     * Constructor for a new, empty structure constant object (seeded with zero vectors)
     */
    public NaryStrucons(Composition metricComposition, int dimens, int arity) {
        this.dimens = dimens;
        this.arity = arity;
        this.metricComposition = metricComposition;
        numEntries = NaryStrucons.calculateStruconsSize(dimens, arity);
        entries = new Vector[numEntries];
        clear();
    }

    /**
     * Copy-constructor that does a deep clone of all entries
     */
    public NaryStrucons(NaryStrucons strucons) {
        dimens = strucons.getDimens();
        arity = strucons.getArity();
        this.metricComposition = strucons.getMetricComposition();
        numEntries = NaryStrucons.calculateStruconsSize(dimens, arity);
        entries = new Vector[numEntries];
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            this.setEntryAt(new Vector(strucons.getEntryAt(entryIdx)), entryIdx); // deep clone every entry
        }
    }


    public Vector[] getEntries() {
        return entries;
    }

    /**
     * shallow set entries (no cloning)
     */
    public void setEntries(Vector[] entries) {
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            this.setEntryAt(entries[entryIdx], entryIdx); // shallow
        }
    }

    public int getDimens() {
        return dimens;
    }

    public int getArity() {
        return arity;
    }

    public Composition getMetricComposition() {
        return metricComposition;
    }

    public Vector getEntryAt(int... entryCoords) {
        return entries[coordsToIndex(entryCoords)];
    }

    public Vector getEntryAt(int entryIdx) {
        return entries[entryIdx];
    }

    public NaryStrucons setEntryAt(Vector vector, int... entryCoords) {
        entries[coordsToIndex(entryCoords)] = vector;
        return this;
    }

    public NaryStrucons setEntryAt(Vector vector, int entryIdx) {
        entries[entryIdx] = vector;
        return this;
    }


    // ----------


    /**
     * Calculate the total number of structure constants.
     */
    public static int calculateStruconsSize(int dimens, int arity) {
        if (arity == 0) return 1;
        if (arity == 1) return dimens;
        if ((arity & 1) == 0) {
            // for even arity, return (dimens^2)^(arity/2)
            return calculateStruconsSize(dimens * dimens, arity / 2);
        } else {
            // for odd arity, return dimens*(dimens^2)^arity/2
            return dimens * calculateStruconsSize(dimens * dimens, arity / 2);
        }
    }


    // ----------


    /**
     * Perform multiplication given a set of structure constants
     */
    public Vector multiply(Vector... factors) {

        Vector product = new Vector(metricComposition, dimens);
        int[] entryCoords = new int[arity]; // start at [0, ..., 0]

        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            // the coordinates of the strucons at this index correspond to the coefficients of the factors

            double coefProduct = 1.;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                coefProduct *= factors[factorIdx].getCoefAt(entryCoords[factorIdx]);
            }
            product.addScaled(entries[entryIdx], coefProduct);

            entryCoords = coordsIncrement(entryCoords);
        }

        return product;
    }

    /**
     * Perform binary multiplication given the structure constants, between a pair of vectors at given factorIdx values.
     * <p>
     * The fixedEntryCoords array specifies the slice of the structure constants to be used. Entries in
     * fixedEntryCoords that correspond to factorIdx1 and factorIdx2 are ignored.
     */
    public Vector multiplyBinary(Vector factor1, int factorIdx1, Vector factor2, int factorIdx2,
                                 int[] fixedEntryCoords) {

        Vector product = new Vector(metricComposition, dimens);
        int[] entryCoords = new int[arity]; // start at [0, ..., 0]

        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            // the coordinates of the strucons at this index correspond to the coefficients of the factors

            boolean isInSlice = true;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                if ((factorIdx != factorIdx1) && (factorIdx != factorIdx2)) {
                    if (entryCoords[factorIdx] != fixedEntryCoords[factorIdx]) {
                        isInSlice = false;
                        break;
                    }
                }
            }

            if (isInSlice) {
                // structure constants within the selected slice; perform product
                double coefProduct = 1.;
                coefProduct *= factor1.getCoefAt(entryCoords[factorIdx1]);
                coefProduct *= factor2.getCoefAt(entryCoords[factorIdx2]);
                product.addScaled(entries[entryIdx], coefProduct);
            }

            entryCoords = coordsIncrement(entryCoords);
        }

        return product;
    }

    /**
     * Scales all entries of the structure constants to a certain length. Any entries that were zero
     * vectors will be set to a new random number of the given length.
     */
    public NaryStrucons normalize(double length) {
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            entries[entryIdx].setLength(length);
        }
        return this;
    }

    /**
     * Sets values with magnitude smaller than threshold to 0.
     */
    public NaryStrucons scrub(double threshold) {
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            entries[entryIdx].scrub(threshold);
        }
        return this;
    }

    /**
     * Given a set of coordinates, returns the corresponding array index value for entries.
     * The entries corresponding to the first factor use increments of 1 (up to the vector space dimensionality),
     * entries for the second factor use increments of dimens, the third factor uses increments of (dimens * dimens),
     * and so on.
     */
    public int coordsToIndex(int... entryCoords) {
        int increment = 1;
        int entryIdx = 0;
        for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
            entryIdx += (entryCoords[factorIdx] * increment);
            increment *= dimens;
        }
        return entryIdx;
    }

    /**
     * Given an index to the entries here, returns the corresponding set of coordinates. Each entry in the coords
     * array corresponds to a D-dimensional factor in the N-ary product.
     */
    public int[] indexToCoords(int entryIdx) {
        int[] entryCoords = new int[arity];
        int idxRemainder = entryIdx;
        for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
            entryCoords[factorIdx] = idxRemainder % dimens;
            idxRemainder = idxRemainder / dimens;
        }
        return entryCoords;
    }

    /**
     * Fills all structure constants with new zero vectors
     */
    public NaryStrucons clear() {
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            entries[entryIdx] = new Vector(metricComposition, dimens); // each entry gets a new, distinct instance of a zero vector
        }
        return this;
    }

    /**
     * Helper to get the coordinates that correspond to an increment in index value. Each entry in the
     * incoming coords[] array represents a dimension of a factor, i.e., the coords[0] value is a vector
     * space dimension of the first factor (vector), coords[1] is a dimension of the second factor, and to so.
     * The incoming coords[] array must point to valid entries, i.e., in the range
     * [ 0..(dimens-1), ..., 0..(dimens-1) ].
     * <p>
     * The incoming array will be modified in place, as well as returned for convenience (to use as builder).
     */
    public int[] coordsIncrement(int... entryCoords) {
        boolean ctd = true;
        int factorIdx = 0;
        while (ctd && (factorIdx < arity)) {
            int thisCoord = entryCoords[factorIdx] + 1;
            if (thisCoord == dimens) {
                entryCoords[factorIdx] = 0;
                factorIdx++;
            } else {
                entryCoords[factorIdx] = thisCoord;
                ctd = false;
            }
        }
        return entryCoords;
    }

    /**
     * Transforms all structure constants according to a given transformation matrix (i.e. perform an
     * affine transformation in the D-dimensional vector space on each of these vectors).
     */
    public NaryStrucons transformAllEntries(double[][] transformationMatrix) {
        for (Vector entry : entries) {
            entry.transform(transformationMatrix);
        }
        return this;
    }


    // ----------


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int[] entryCoords = new int[arity];
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            sb.append("coord ").append(Arrays.toString(entryCoords)).append(": ");
            sb.append(entries[entryIdx]).append("\n");
            entryCoords = coordsIncrement(entryCoords);
        }
        return sb.toString();
    }

}
