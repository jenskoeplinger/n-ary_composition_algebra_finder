/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.model;

import nary.composition.Composition;
import nary.composition.impl.CompEuclidean;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

/**
 * Vector with double[] coefficients, representing a number in D ("dimens") real dimensions.
 * <p>
 * Methods here return this object instance (where reasonably) so that they can be invoked
 * as builder. Unless otherwise specified, they act on the object's in-memory representation
 * to avoid creating unneeded deep clones.
 */
public class Vector {

    private static Random random;

    private final int dimens;

    private final double[] coefs;

    private final Composition metricComposition;


    // ----------


    public Vector(Composition metricComposition, int dimens) {
        coefs = new double[dimens];
        this.dimens = dimens;
        this.metricComposition = metricComposition;
    }

    public Vector(Composition metricComposition, double... coefs) {
        this.coefs = coefs;
        this.dimens = coefs.length;
        this.metricComposition = metricComposition;
    }

    /**
     * Copy-constructor that clones the coefficients array.
     */
    public Vector(Vector vector) {
        this.coefs = vector.getCoefs().clone();
        this.dimens = coefs.length;
        this.metricComposition = vector.getMetricComposition();
    }


    public double[] getCoefs() {
        return coefs;
    }

    public int getDimens() {
        return dimens;
    }

    public Composition getMetricComposition() {
        return metricComposition;
    }

    public double getCoefAt(int axis) {
        return coefs[axis];
    }

    public void setCoefAt(double coef, int axis) {
        coefs[axis] = coef;
    }

    public static void setRandom(Random random) {
        Vector.random = random;
    }


    // ----------


    /**
     * Sets this number to a pseudo-random vector of a given length.
     */
    public Vector setRandom(double length) {

        if (length == 0) {
            return setLength(0);
        }

        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] = random.nextGaussian();
        }

        return setLength(Math.sqrt(length));
    }

    public double getLength() {
        return metricComposition.metricOf(metricComposition.calculate(this), dimens);
    }

    /**
     * Sets this number to a vector of a given length, preserving its direction.
     */
    public Vector setLength(double length) {
        if (length == 0) {
            for (int axis = 0; axis < dimens; axis++) {
                coefs[axis] = 0;
            }
            return this;
        }

        double l = getLength();

        if (l == 0) {
            return setRandom(length); // edge case; set prior 0 vector to random vector of given length
        }

        double scale = length / l;

        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] = coefs[axis] * scale;
        }

        return this;
    }

    /**
     * Adds another number. Caller must ensure it has the same dimensionality.
     */
    public Vector add(Vector addend) {
        if (addend == null) return this;
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] += addend.getCoefAt(axis);
        }
        return this;
    }

    /**
     * Adds another number, scaled by a factor (convenience method to avoid creating a new object instance,
     * calling scalarMult(...), and then adding).
     */
    public Vector addScaled(Vector addend, double scaleFactor) {
        if (addend == null) return this;
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] += (addend.getCoefAt(axis) * scaleFactor);
        }
        return this;
    }

    /**
     * Adds a double offset to a specified coefficient
     */
    public Vector addAt(int axis, double addend) {
        coefs[axis] += addend;
        return this;
    }

    /**
     * Subtracts another number. Caller must ensure it has the same dimensionality.
     */
    public Vector subtract(Vector subtrahend) {
        if (subtrahend == null) return this;
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] -= subtrahend.getCoefAt(axis);
        }
        return this;
    }

    /**
     * Scalar multiplication
     */
    public Vector scalarMult(double factor) {
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] *= factor;
        }
        return this;
    }

    /**
     * Set to zero (without allocating space for a new array)
     */
    public Vector setZero() {
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] = 0;
        }
        return this;
    }

    /**
     * Negate all coefficients
     */
    public Vector negate() {
        for (int axis = 0; axis < dimens; axis++) {
            coefs[axis] = -coefs[axis];
        }
        return this;
    }

    /**
     * Sets values with magnitude smaller than threshold to 0. Make sure the coefficients are small
     * and threshold is greater than 1.e-15
     */
    public Vector scrub(double threshold) {
        double invThres = Math.round(7.e-16 + 1. / threshold);
        for (int axis = 0; axis < dimens; axis++) {
            long tmp = Math.round(coefs[axis] * invThres);
            coefs[axis] = ((double) tmp) / invThres;
        }
        return this;
    }

    /**
     * Transforms this number by a given NxN matrix (i.e. perform an affine transformation in the D-dimensional
     * vector space).
     */
    public Vector transform(double[][] transformationMatrix) {
        double[] newCoefs = coefs.clone(); // make a clone of the coefficients
        for (int axisRow = 0; axisRow < dimens; axisRow++) {
            double newCoef = 0;
            for (int axisCol = 0; axisCol < dimens; axisCol++) {
                newCoef += transformationMatrix[axisCol][axisRow] * newCoefs[axisCol];
            }
            coefs[axisRow] = newCoef; // directly assign the new coefficient
        }
        return this;
    }


    // ----------


    @Override
    public String toString() {
        return Arrays.toString(coefs);
    }

    /**
     * Object-equals, uses strict equals() on double coeficients. Don't use
     * for algebraic equals (which needs to use a precision threshold).
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector that = (Vector) o;
        return dimens == that.dimens &&
                Arrays.equals(coefs, that.coefs); // strict equals() on double
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(dimens);
        result = 31 * result + Arrays.hashCode(coefs);
        return result;
    }
}
