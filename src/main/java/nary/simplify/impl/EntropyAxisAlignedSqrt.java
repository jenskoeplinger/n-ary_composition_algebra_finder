/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.model.NaryStrucons;
import nary.model.Vector;
import nary.simplify.Entropy;

/**
 * Like EntropyAxisAligned, but weighs deviations from perfect using the sqrt function.
 *
 * @see EntropyAxisAligned
 */
public class EntropyAxisAlignedSqrt implements Entropy {

    @Override
    public double calculate(NaryStrucons strucons) {

        double entropy = 0;

        for (Vector entry : strucons.getEntries()) {
            for (double coef : entry.getCoefs()) {
                coef = Math.abs(2. * coef);
                if (coef > 1.) coef = 2. - coef;
                entropy += Math.sqrt(Math.abs(coef));
            }
        }

        double numCoefs = (double) (strucons.getEntries().length * strucons.getDimens());
        return Math.min(Math.max(entropy / numCoefs, 0), 1.);
    }

}
