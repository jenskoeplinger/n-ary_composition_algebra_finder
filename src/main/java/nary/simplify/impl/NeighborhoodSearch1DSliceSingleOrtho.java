/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.helper.MatrixMath;
import nary.helper.NaryMath;
import nary.model.NaryStrucons;
import nary.simplify.NeighborhoodSearch;

import java.util.Random;

/**
 * Neighborhood search implementation that applies an arbitrary orthogonal transformation to a random one
 * dimensional subspace of the N-ary structure constants in D dimensions. The rotation angle will be random
 * in the range [-pi * strength ... pi * strength]. Rotations will be elements of O(N), applied on a randomly
 * selected one dimensional structure constants slice (corresponding to one of the factors in the N-ary product).
 */
public class NeighborhoodSearch1DSliceSingleOrtho implements NeighborhoodSearch {

    private Random random;

    @Override
    public void step(NaryStrucons strucons, double strength) {

        int dimens = strucons.getDimens();
        int arity = strucons.getArity();

        // lower the effective strength by the root of the number of rotations (random-walk alike):
        double boundedGaussian = Math.min(Math.max(random.nextGaussian(), -1.), 1.); // cut into the [-1...1] interval
        double effectiveStrength = boundedGaussian * strength / ((double) (dimens - 1));

        double[][] rotationMatrix = MatrixMath.createRandomO2Rotation(random, dimens, effectiveStrength);

        int factorIdx = (int) (random.nextDouble() * ((double) arity));

        // rotate the 1D structure constants slice along O(N)
        NaryMath.transformStrucons(strucons, rotationMatrix, factorIdx);
    }

    @Override
    public void setRandom(Random random) {
        this.random = random;
    }

}
