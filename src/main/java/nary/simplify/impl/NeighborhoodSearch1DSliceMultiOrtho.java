/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.model.NaryStrucons;
import nary.simplify.NeighborhoodSearch;

import java.util.Random;

/**
 * Neighborhood search implementation that applies a number of arbitrary orthogonal transformations to a random one
 * dimensional subspace of the N-ary structure constants in D dimensions. The rotation angle will be random in the
 * range [-pi * strength ... pi * strength]. Rotations will be elements of O(N), applied on a randomly selected
 * one dimensional structure constants slice (corresponding to one of the factors in the N-ary product).
 */
public class NeighborhoodSearch1DSliceMultiOrtho implements NeighborhoodSearch {

    private Random random;

    @Override
    public void step(NaryStrucons strucons, double strength) {

        int dimens = strucons.getDimens();
        int totalSteps = dimens * 2;
        double effectiveStrength = strength / ((double) totalSteps);
        NeighborhoodSearch1DSliceSingleOrtho singleOrtho = new NeighborhoodSearch1DSliceSingleOrtho();
        singleOrtho.setRandom(random);

        for (int stepCnt = 0; stepCnt < totalSteps; stepCnt++) {
            singleOrtho.step(strucons, effectiveStrength);
        }

    }

    @Override
    public void setRandom(Random random) {
        this.random = random;
    }

}
