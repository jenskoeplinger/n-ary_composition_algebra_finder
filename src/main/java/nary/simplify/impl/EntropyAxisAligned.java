/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.model.Vector;
import nary.model.NaryStrucons;
import nary.simplify.Entropy;

/**
 * Given N-ary structure constants in D dimensions, considers them "more ordered" the more of its coefficients
 * align with the orthonormal basis elements of the underlying vector space, R^D. Structure constants
 * that are in {-1, 0, 1} are therefore assumed to be "perfectly ordered" (0 entropy), and entries that are
 * {-1/2, +1/2} have the highest disorder (1 entropy).
 * <p>
 * This means that the highest contribution to the entropy that a single entry may give is D, i.e., a contribution
 * of 1 from each of the entrie's coefficients. This makes the function suitable for structure constants that
 * are aligned with the integral lattice in D dimensions, but unsuitable for structure constants that align with
 * other lattices. For example, the F4 lattice in 4 dimension has entries ( 0.5, 0.5, 0.5, 0.5 ) (incl. reflections)
 * which would be avoided here.
 * <p>
 * Note: Due to Java double precision, the returned entropy value may be slightly negative for a perfect solution.
 * It is the caller's responsibility to implement a proper abort criterion, based on the absolute return value.
 */
public class EntropyAxisAligned implements Entropy {

    @Override
    public double calculate(NaryStrucons strucons) {

        double entropy = 0;

        for (Vector entry : strucons.getEntries()) {
            for (double coef : entry.getCoefs()) {
                coef = Math.abs(2. * coef);
                if (coef > 1.) coef = 2. - coef;
                entropy += coef;
            }
        }

        double numCoefs = (double) (strucons.getEntries().length * strucons.getDimens());
        return Math.min(Math.max(entropy / numCoefs, 0), 1.);
    }

}
