/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.helper.MatrixMath;
import nary.model.NaryStrucons;
import nary.simplify.NeighborhoodSearch;

import java.util.Random;

/**
 * Given N-ary structure constants in D dimensions and rotation strength, randomly picks a single axis of R^D and
 * rotates each structure constants entry by a maximum random angle of [ -pi * strength, pi * strength ].
 */
public class NeighborhoodSearchEntriesSingleRotation implements NeighborhoodSearch {

    private Random random;

    @Override
    public void step(NaryStrucons strucons, double strength) {

        int dimens = strucons.getDimens();
        double boundedGaussian = Math.min(Math.max(random.nextGaussian(), -1.), 1.); // cut into the [-1...1] interval
        double effectiveStrength = strength * (1. + boundedGaussian);

        double[][] rotationMatrix = MatrixMath.createRandomSO2Rotation(random, dimens, effectiveStrength);

        // rotate every entry in the structure constants by this angle
        strucons.transformAllEntries(rotationMatrix);
    }

    @Override
    public void setRandom(Random random) {
        this.random = random;
    }

}
