/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.model.NaryStrucons;
import nary.model.Vector;
import nary.simplify.Entropy;

import java.util.Arrays;

/**
 * Given N-ary structure constants in D dimensions, considers it "more ordered" the smaller the weighted
 * difference between every successive coefficient pair in the ordered series of all coefficient absolutes is.
 */
public class EntropySortedPairWeightedDistance implements Entropy {

    @Override
    public double calculate(NaryStrucons strucons) {

        int dimens = strucons.getDimens();
        int arity = strucons.getArity();
        int sortedCoefsSize = NaryStrucons.calculateStruconsSize(dimens, arity + 1);
        double[] sortedCoefs = new double[sortedCoefsSize];
        int sortedCoefsIdx = 0;

        // (1) build an array of all coefficient absolutes
        for (Vector entry : strucons.getEntries()) {
            for (double coef : entry.getCoefs()) {
                sortedCoefs[sortedCoefsIdx] = Math.abs(coef);
                sortedCoefsIdx++;
            }
        }

        assert sortedCoefsIdx == sortedCoefsSize;

        // (2) sort it
        Arrays.sort(sortedCoefs);

        // (3) sum up the distance squares between each successive pair
        double entropy = 0;
        for (sortedCoefsIdx = 1; sortedCoefsIdx < sortedCoefsSize; sortedCoefsIdx++) {
            double coefDiff = sortedCoefs[sortedCoefsIdx] - sortedCoefs[sortedCoefsIdx - 1];
            entropy += (coefDiff * coefDiff);
        }

        entropy = 1. - entropy;

        return Math.min(Math.max(entropy, 0), 1.);
    }

}
