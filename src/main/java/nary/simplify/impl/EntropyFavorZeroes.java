/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.model.NaryStrucons;
import nary.model.Vector;
import nary.simplify.Entropy;

/**
 * Given N-ary structure constants in D dimensions, considers it "more ordered" the more of its coefficients
 * are at or near zero.
 */
public class EntropyFavorZeroes implements Entropy {

    @Override
    public double calculate(NaryStrucons strucons) {

        double entropy = 0;

        for (Vector entry : strucons.getEntries()) {
            for (double coef : entry.getCoefs()) {
                coef = Math.abs(coef);
                if (coef >= 0.1) {
                    entropy += 1.;
                } else {
                    entropy += (coef / 10.);
                }
            }
        }

        double numCoefs = (double) (strucons.getEntries().length * strucons.getDimens());
        return Math.min(Math.max(entropy / numCoefs, 0), 1.);
    }

}
