/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify.impl;

import nary.helper.MatrixMath;
import nary.helper.NaryMath;
import nary.model.NaryStrucons;
import nary.simplify.NeighborhoodSearch;

import java.util.Random;

/**
 * Neighborhood search implementation that applies an arbitrary rotation to a random two dimensional subspace
 * of the N-ary structure constants in D dimensions. The rotation angle will be random in the range
 * [-pi * strength ... pi * strength]. Rotations will be elements of SO(N), applied on a randomly selected
 * two dimensional structure constants slice.
 */
public class NeighborhoodSearch2DSliceSingleRotation implements NeighborhoodSearch {

    private Random random;

    @Override
    public void step(NaryStrucons strucons, double strength) {

        int dimens = strucons.getDimens();
        int arity = strucons.getArity();

        // lower the effective strength by the root of the number of rotations (random-walk alike):
        double boundedGaussian = Math.min(Math.max(random.nextGaussian(), -1.), 1.); // cut into the [-1...1] interval
        double effectiveStrength = boundedGaussian * strength / ((double) (dimens - 1));

        double[][] rotationMatrix = MatrixMath.createRandomSO2Rotation(random, dimens, effectiveStrength);

        int factorIdx1 = (int) (random.nextDouble() * ((double) arity));
        int factorIdx2 = (int) (random.nextDouble() * ((double) (arity - 1)));
        if (factorIdx1 == factorIdx2) factorIdx2 = arity - 1;

        // rotate the two dimensional structure constants slice along SO(N)
        NaryMath.transformStrucons(strucons, rotationMatrix, factorIdx1, factorIdx2);
    }

    @Override
    public void setRandom(Random random) {
        this.random = random;
    }

}
