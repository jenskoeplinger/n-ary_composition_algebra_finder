/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify;

/**
 * Container to hold structure constants at a given index, implementing a comparator from the entropy of the
 * structure constants.
 */
public class StruconsIndexEntropyComparableContainer implements Comparable {

    private double entropy;
    private int struconsIndex;
    private long iterationsSinceLastImprovement = 0;

    public StruconsIndexEntropyComparableContainer(int struconsIndex, double entropy) {
        this.entropy = entropy;
        this.struconsIndex = struconsIndex;
    }

    public double getEntropy() {
        return entropy;
    }

    public void setEntropy(double entropy) {
        this.entropy = entropy;
    }

    public int getStruconsIndex() {
        return struconsIndex;
    }

    public long getAndIncrementIterationsSinceLastImprovement() {
        long returnVal = iterationsSinceLastImprovement;
        iterationsSinceLastImprovement++;
        return returnVal;
    }

    public void decrementIterationsSinceLastImprovement() {
        if (iterationsSinceLastImprovement > 0) {
            iterationsSinceLastImprovement /= 2;
        }
    }

    @Override
    public String toString() {
        return "(" + entropy + ", " + struconsIndex + ")";
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) return -1;
        double thatVal = ((StruconsIndexEntropyComparableContainer) o).getEntropy();
        return Double.compare(getEntropy(), thatVal);
    }

}
