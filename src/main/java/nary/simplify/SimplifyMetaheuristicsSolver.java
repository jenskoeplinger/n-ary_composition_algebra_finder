/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify;

import nary.NaryCompositionFinderApp;
import nary.composition.Composition;
import nary.helper.IntermittentLogger;
import nary.helper.NaryMath;
import nary.model.NaryStrucons;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;

/**
 * Simplifies given structure constants by making their entries more human friendly.
 * <p>
 * The algorithm is a population-based variable neighborhood search. It begins with a given base set of structure
 * constants, and calculates its (pseudo-)entropy according to the provided entropy implementation. This
 * (pseudo-)entropy value becomes the step size to build the initial population of structure constants, by invoking
 * the provided neighborhood search algorithm once for each new member in the population.
 * <p>
 * The algorithm then iterates through the structure constants population as follows, for each member:
 * - calculate the (pseudo-)entropy of this member
 * - begin calculation of step size with this entropy value
 * - multiply the step size by an (optional) relativeStepSize constant
 * - in the first few iterations, let this be the step size
 * - with increasing number of iterations, gradually attempt both larger and smaller step sizes
 * - on even iteration counts, attempt larger step sizes; on odd iteration counts, attempt smaller step sizes
 * - the rate at which these adjusted step sizes deviate from the initial step size is given by a Gaussian
 * - the weight (half-width) of the Gaussian is a constant (iterationsUnimprovedStepSizeAdjust)
 * - the step sizes for even/odd iterations will approach 1. (random) and 0 (no change), respectively
 * <p>
 * The iteration will abort if one of the following two conditions are met:
 * - The iteration counter exceeds a given value, or
 * - the lowest entropy of a set of structure constants in the population falls below a specified value.
 * <p>
 * The abort criterion that is based on iteration counter is implemented to give the overall search algorithm
 * a chance to tune the structure constants, to ensure that cumulative rounding errors don't deviate too
 * far from it representing a composition algebra.
 */
public class SimplifyMetaheuristicsSolver {

    private final static Logger logger = LoggerFactory.getLogger(SimplifyMetaheuristicsSolver.class);

    private final Entropy entropyImpl;

    private final Composition compositionImpl;

    private final NaryStrucons baseStrucons;

    private final NeighborhoodSearch neighborhoodSearchImpl;

    private final int populationSize;

    private final double relativeStepSize;

    private final double iterationsUnimprovedStepSizeAdjust;

    private final double abortMaxEntropy;

    private final double struconsScrubPrecision;

    private final long abortMaxIterations;

    private final long minLogTimeInterval;

    private final NaryStrucons[] struconsPopulation;

    private final StruconsIndexEntropyComparableContainer[] struconsPopulationOrder;

    /**
     * Iteration counter
     */
    private long iterationCnt = -1L;

    /**
     * Whether to abort the iteration (if set to true, iteration will finish soon)
     */
    private boolean abortIteration = false;

    /**
     * Whether the iteration has actually finished.
     */
    private boolean iterationHasFinished = false;

    /**
     * If the best entropy of the iteration fell below the threshold, we're done.
     */
    private boolean bestEntropyFound = false;

    // ----------


    /**
     * Supply all variables required
     *
     * @see NaryCompositionFinderApp for documentation on the constructor arguments.
     */
    public SimplifyMetaheuristicsSolver(
            Entropy entropyImpl,
            Composition compositionImpl,
            NaryStrucons baseStrucons,
            NeighborhoodSearch neighborhoodSearchImpl,
            int populationSize,
            double relativeStepSize,
            double iterationsUnimprovedStepSizeAdjust,
            double abortMaxEntropy,
            double struconsScrubPrecision,
            long abortMaxIterations,
            long minLogTimeInterval
    ) {
        this.entropyImpl = entropyImpl;
        this.compositionImpl = compositionImpl;
        this.baseStrucons = baseStrucons;
        this.neighborhoodSearchImpl = neighborhoodSearchImpl;
        this.populationSize = populationSize;
        this.relativeStepSize = relativeStepSize;
        this.iterationsUnimprovedStepSizeAdjust = iterationsUnimprovedStepSizeAdjust;
        this.abortMaxEntropy = abortMaxEntropy;
        this.struconsScrubPrecision = struconsScrubPrecision;
        this.abortMaxIterations = abortMaxIterations;
        this.minLogTimeInterval = minLogTimeInterval;

        struconsPopulation = new NaryStrucons[populationSize];
        struconsPopulationOrder = new StruconsIndexEntropyComparableContainer[populationSize];
    }

    /**
     * Execute the iterative solver algorithm.
     */
    public void solve() {

        logger.info("Solver starting at " + (new Date()));

        // initialize the population of sets of structure constants:
        // - make a clone of the base set of structure constants
        // - rotate the cloned set along a random angle and store it in the struconsPopulation array
        // - calculate its entropy and initialize the struconsPopulationOrder array

        double baseEntropy = entropyImpl.calculate(baseStrucons);
        logger.info("Entropy of base set of structure constants: " + baseEntropy);

        for (int popIdx = 0; popIdx < populationSize; popIdx++) {
            // clone the base set of structure constants (clone every entry):
            struconsPopulation[popIdx] = new NaryStrucons(baseStrucons);
            if (popIdx > 0) {
                neighborhoodSearchImpl.step(struconsPopulation[popIdx], baseEntropy);
            }
            double entropy = entropyImpl.calculate(struconsPopulation[popIdx]);
            struconsPopulationOrder[popIdx] = new StruconsIndexEntropyComparableContainer(popIdx, entropy);

            if (logger.isDebugEnabled()) {
                logger.debug("popIdx=" + popIdx + ": strucons created:\n" + struconsPopulation[popIdx]);
                logger.debug("popIdx=" + popIdx + ": struconsPopulationOrder[popIdx]=" + struconsPopulationOrder[popIdx]);

                double residualTest = NaryMath.calculateRandomResiduals(struconsPopulation[popIdx], compositionImpl, 1);
                logger.debug("residualTest=" + residualTest + " (should be 0)");
            }

        }

        logger.info("Random population of sets of structure constants initialized; population size: " + populationSize);

        iterationCnt = 0;
        IntermittentLogger iLog = new IntermittentLogger(logger, minLogTimeInterval);

        while (!abortIteration) {

            // sort the population of sets of structure constants by entropy
            Arrays.sort(struconsPopulationOrder);

            iLog.log("[" + iterationCnt + "] sorted struconsPopulationOrder = "
                    + Arrays.toString(struconsPopulationOrder), iterationCnt);

            // loop through the population, from lowest to highest entropy
            for (int popIdx = 0; popIdx < populationSize; popIdx++) {

                StruconsIndexEntropyComparableContainer thisStruconsOrderedContainer = struconsPopulationOrder[popIdx];
                int struconsIndex = thisStruconsOrderedContainer.getStruconsIndex();
                double oldEntropy = thisStruconsOrderedContainer.getEntropy();
                long iterationsSinceLastImprovement = thisStruconsOrderedContainer.getAndIncrementIterationsSinceLastImprovement();
                double weightedIterationsSinceLastImprovement = iterationsSinceLastImprovement * oldEntropy / iterationsUnimprovedStepSizeAdjust;
                double cumulativeStepSizeAdjust = Math.exp(-(weightedIterationsSinceLastImprovement * weightedIterationsSinceLastImprovement));
                if (oldEntropy < abortMaxEntropy) {
                    cumulativeStepSizeAdjust = 0; // don't ruin a perfect thing
                } else if ((iterationsSinceLastImprovement % 2) == 0) {
                    // gradually increase the iteration step size on even iteration counts:
                    // flip the Gaussian, have it 0 at x=0, and (1. - oldEntropy) for x against infinity
                    cumulativeStepSizeAdjust = (1. - cumulativeStepSizeAdjust) * (1. - oldEntropy);
                } else {
                    // gradually decrease the iteration step size on odd iteration counts:
                    // have it 0 at x=0, and -oldEntropy for x against infinity
                    cumulativeStepSizeAdjust = (cumulativeStepSizeAdjust - 1.) * oldEntropy;
                }
                double stepSize = (oldEntropy + cumulativeStepSizeAdjust) * relativeStepSize;

                NaryStrucons strucons = new NaryStrucons(struconsPopulation[struconsIndex]); // create a clone

                // perform a neighborhood search, with step size a function of relative entropy squared
                neighborhoodSearchImpl.step(strucons, stepSize);

                // update the entropy
                double newEntropy = entropyImpl.calculate(strucons);

                if (newEntropy < oldEntropy) {
                    // better structure constants found
                    struconsPopulation[struconsIndex] = strucons;
                    thisStruconsOrderedContainer.setEntropy(newEntropy);
                    thisStruconsOrderedContainer.decrementIterationsSinceLastImprovement();
                }

            }

            // all sets of structure constants have been nudged
            iterationCnt++;

            double bestEntropy = struconsPopulationOrder[0].getEntropy();

            if (iterationCnt >= abortMaxIterations) {
                // temporarily abort iteration in order to avoid compounding rounding errors
                logger.info("Iteration count " + iterationCnt
                        + " has reached abort criterion (" + abortMaxIterations + ")");
                abortIteration = true;
                if ((baseEntropy - bestEntropy) < abortMaxEntropy) {
                    // no significant improvement this run; consider structure constants "simplified"
                    // (lowest possible entropy found)
                    logger.info("Best entropy " + bestEntropy
                            + " not significantly better than starting entropy " + baseEntropy
                            + " (less than " + abortMaxEntropy
                            + "); consider best possible set of structure constants found "
                            + "(given the algorithm / implementation)");
                    bestEntropyFound = true;
                }
            }

            if (bestEntropy < abortMaxEntropy) {
                // structure constants considered "simplified" (overall best possible entropy value found)
                logger.info("Best entropy " + bestEntropy
                        + " is smaller than abort (" + abortMaxEntropy + "), consider global optimum found");
                abortIteration = true;
                bestEntropyFound = true;
            }
        }

        NaryStrucons bestStrucons = struconsPopulation[struconsPopulationOrder[0].getStruconsIndex()];

        double bestEntropy = entropyImpl.calculate(bestStrucons);
        logger.info("Best structure constants:\n\n" + bestStrucons);
        logger.info("Entropy of best structure constants: " + bestEntropy);

        NaryStrucons scrubbedStrucons = new NaryStrucons(struconsPopulation[struconsPopulationOrder[0].getStruconsIndex()]);
        scrubbedStrucons.scrub(struconsScrubPrecision);
        logger.info("Best structure constants (scrubbed at " + struconsScrubPrecision + " precision):\n\n" + scrubbedStrucons);
        logger.info("Entropy of scrubbed best structure constants: " + entropyImpl.calculate(scrubbedStrucons));

        logger.info("(best entropy considered found: " + bestEntropyFound + ")");
        double residualTest = NaryMath.calculateRandomResiduals(bestStrucons, compositionImpl, 100);
        logger.info("residuals test on structure constants (100 samples; should be near 0): " + residualTest);

        // copy the entries from best structure constants into the base set of structure constants
        // (for continued iteration / refinement)
        baseStrucons.setEntries(scrubbedStrucons.getEntries());

        logger.info("Solver finished at " + (new Date()));

        iterationHasFinished = true;
    }


    // ----------


    public long getIterationCnt() {
        return iterationCnt;
    }

    public boolean getIterationHasFinished() {
        return iterationHasFinished;
    }

    /**
     * If running on a separate thread, call this method to force an iteration abort
     */
    public void abortIteration() {
        this.abortIteration = true;
    }

    /**
     * Returns true if the solver found an entropy smaller than the threshold
     */
    public boolean wasBestEntropyFound() {
        return bestEntropyFound;
    }

}
