/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify;

import nary.model.NaryStrucons;

/**
 * The (pseudo-)entropy of N-ary structure constants in D dimensions. The structure constants are assumed to be
 * generalized D-dimensional multiplication tables, where each entry is a vector in R^D as well.
 * <p>
 * The number returned by the implementing class is to be lower the more the given structure constants
 * satisfy the sought-for degree of order ("simplification").
 * <p>
 * An entropy value of 0 indicates perfect order. A value of 1 total disorder. Values must not be
 * greater than 1. or smaller than 0.
 */
public interface Entropy {

    double calculate(NaryStrucons strucons);

}
