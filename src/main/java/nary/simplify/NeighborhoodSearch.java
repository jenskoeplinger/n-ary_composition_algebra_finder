/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.simplify;

import nary.model.NaryStrucons;

import java.util.Random;

/**
 * Executes a heuristical neighborhood search step from given N-ary structure constants. Step size can be
 * tuned by providing a strength parameter [0..1].
 */
public interface NeighborhoodSearch {

    /**
     * Execute a search step [0..1].
     * <p>
     * A value of 0 should leave the structure constants unchanged, and a value of 1 should make a change that is
     * random across the entire configuration space.
     */
    void step(NaryStrucons strucons, double strength);

    void setRandom(Random random);

}
