/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition.impl;

import nary.composition.Composition;
import nary.model.Vector;

public class CompEuclideanMinkowskian implements Composition {

    private final CompEuclidean compEuclidean = new CompEuclidean();
    private final CompMinkowskian compMinkowskian = new CompMinkowskian();

    @Override
    public double calculate(Vector vector) {
        return compEuclidean.calculate(vector) * compMinkowskian.calculate((vector));
    }

    @Override
    public double metricOf(double composition, double dimens) {
        return Math.pow(Math.abs(composition), 0.25);
    }

}
