/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition.impl;

import nary.composition.Composition;
import nary.model.Vector;

public class CompMixedTaxi implements Composition {

    private final CompTaxi compTaxi = new CompTaxi();
    private final CompSplitTaxi compSplitTaxi = new CompSplitTaxi();

    @Override
    public double calculate(Vector vector) {
        return compTaxi.calculate(vector) * compSplitTaxi.calculate((vector));
    }

    @Override
    public double metricOf(double composition, double dimens) {
        return Math.sqrt(Math.abs(composition));
    }

}
