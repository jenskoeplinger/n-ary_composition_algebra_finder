/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition.impl;

import nary.composition.Composition;
import nary.model.Vector;

public class CompSplitTaxi implements Composition {

    @Override
    public double calculate(Vector vector) {
        double result = 0;
        int dimens = vector.getDimens();
        int dimensHalf = dimens / 2;
        for (int i = 0; i < dimens; i++) {
            double coef = vector.getCoefAt(i);
            if (i < dimensHalf) {
                result += Math.abs(coef);
            } else {
                result -= Math.abs(coef);
            }
        }
        return result;
    }

    @Override
    public double metricOf(double composition, double dimens) {
        return Math.abs(composition);
    }

}
