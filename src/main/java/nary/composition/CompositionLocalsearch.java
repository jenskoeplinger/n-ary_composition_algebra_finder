/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition;

import nary.model.NaryStrucons;
import nary.model.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Implement a single local search step, in the process of finding an algebra that fits a given composition.
 * <p>
 * Given the residual between actual product of compositions and the
 * composition of the product, nudges each of the N-ary structure constants into
 * "the right" direction. From the given factors, the larger coefficient tuples are assumed
 * to contribute large to the error (the "residual") and are higher weighted.
 * Smaller coefficient tuples are suppressed.
 */
public class CompositionLocalsearch {

    private static Logger logger = LoggerFactory.getLogger(CompositionLocalsearch.class);

    private static final double GRADIENT_MAX_STEP_SIZE = 1.e-8; // size of delta for estimating local gradient

    private static final double GRADIENT_MIN_STEP_SIZE = 1.e-15; // minimum step size given by Java "double" precision

    /**
     * Executes a single local search step
     *
     * @param strucons              The N-ary multiplication rules, as structure constants.
     * @param numbers               N numbers (each D-dimensional) that were multiplied.
     * @param product               Product of the numbers according to the structure constants.
     * @param composition           Function to use for testing multiplicative composition
     * @param metricComposition     Function to use for calculating metric (i.e. unit distance from the origin)
     * @param productOfComps        Product of the composition function on each number.
     * @param residualMovingAverage Moving average of prior residuals
     * @param compOfProduct         Composition function on the product of the numbers.
     * @param learningRate          Base step size to adjust a structure constant by.
     */
    public static void search(
            NaryStrucons strucons,
            Vector product,
            Composition composition,
            Composition metricComposition,
            double productOfComps,
            double compOfProduct,
            double residualMovingAverage,
            double learningRate,
            Vector... numbers) {

        // For all coefficient tuples of the N-ary number product,
        // find the mean absolute.

        double sumAbsCoefProducts = 0;
        double overallResidual = Math.abs(compOfProduct - productOfComps);
        int dimens = strucons.getDimens();
        int arity = strucons.getArity();
        int numEntries = strucons.getEntries().length;

        int[] entryCoords = new int[arity];
        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {
            double coefProduct = 1.;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                coefProduct *= numbers[factorIdx].getCoefAt(entryCoords[factorIdx]);
            }
            sumAbsCoefProducts += Math.abs(coefProduct);
            entryCoords = strucons.coordsIncrement(entryCoords);
        }

        if (sumAbsCoefProducts < .9999999) {
            throw new RuntimeException("sumAbsCoefProducts too small: " + sumAbsCoefProducts
                    + "; from: " + Arrays.toString(numbers));
        }

        double meanAbsCoefProducts = sumAbsCoefProducts / ((double) numEntries);

        double overallResidualOverCoefs = overallResidual / sumAbsCoefProducts;
        double effectiveLearningRate = overallResidualOverCoefs * learningRate;

        // make more and more fine grained steps when we've found a solution, to increase precision
        double gradientStepSize = Math.max(
                Math.min(residualMovingAverage, GRADIENT_MAX_STEP_SIZE),
                GRADIENT_MIN_STEP_SIZE);
        double baseStepSize = effectiveLearningRate / gradientStepSize;

        // Now loop through all coefficient tuples again and find their
        // contribution to the modulus. Find the estimated direction of
        // biggest improvement, and then adjust the structure constants
        // entry in that direction, weighted by the learning rate, the
        // relative absolute value of the coordinate tuples' product over
        // the overall mean, and the absolute of the overall residual.

        entryCoords = new int[arity];
        Vector testProduct = new Vector(product);
        Vector gradient = new Vector(metricComposition, dimens);

        for (int axis = 0; axis < dimens; axis++) {

            double testCompOfProduct;

            // nudge in positive direction
            testProduct.addAt(axis, gradientStepSize);
            testCompOfProduct = composition.calculate(testProduct);
            double thisAbsResidualPos = Math.abs(productOfComps - testCompOfProduct);
            // nudge in negative direction
            testProduct.addAt(axis, -2. * gradientStepSize);
            testCompOfProduct = composition.calculate(testProduct);
            double thisAbsResidualNeg = Math.abs(productOfComps - testCompOfProduct);

            // determine gradient (positive in the direction of lesser residual)
            gradient.setCoefAt(thisAbsResidualNeg - thisAbsResidualPos, axis);

            // move the test product back to where it was
            testProduct.addAt(axis, gradientStepSize);
        }

        if (logger.isDebugEnabled()) logger.debug("gradient=" + gradient);

        for (int entryIdx = 0; entryIdx < numEntries; entryIdx++) {

            double coefProduct = 1.;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                coefProduct *= numbers[factorIdx].getCoefAt(entryCoords[factorIdx]);
            }

            // Execute step only for significant coefficients (here: higher than mean).
            // This magnifies the effect of errors.
            if (Math.abs(coefProduct) > meanAbsCoefProducts) {
                double thisStepSize = coefProduct * baseStepSize;
                strucons.getEntryAt(entryIdx).addScaled(gradient, thisStepSize);
            }

            // next coordinate
            entryCoords = strucons.coordsIncrement(entryCoords);
        }

        // normalize structure constants to 1. (generally not needed, but may or may not speed up solution finding)
        // strucons.normalize(1.);

    }

}
