/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition;

import nary.NaryCompositionFinderApp;
import nary.helper.IntermittentLogger;
import nary.model.NaryStrucons;
import nary.model.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;

import static nary.composition.CompositionLocalsearch.search;

/**
 * Sovles for a given composition function by iterative adjustments (local neighborhood search) of
 * starting structure constants for N-ary multiplication in D dimensions.
 *
 * @see NaryCompositionFinderApp for documentation on the constructor arguments.
 */
public class CompositionLocalsearchSolver {

    private final static Logger logger = LoggerFactory.getLogger(NaryCompositionFinderApp.class);

    private final int dimens;

    private final int arity;

    private final Composition compositionImpl;

    private final Composition metricCompositionImpl;

    private final NaryStrucons strucons;

    private final double neighborhoodScale;

    private final double residualsMovingAverageRate;

    private final double abortMaxResiduals;

    private final long minLogTimeInterval;

    /**
     * Iteration counter
     */
    private long iterationCnt = -1L;

    /**
     * Whether to abort the iteration (if set to true, iteration will finish soon)
     */
    private boolean abortIteration = false;

    /**
     * Whether the iteration has actually finished.
     */
    private boolean iterationHasFinished = false;

    /**
     * The most recent residual
     */
    private double residual = 2.;

    /**
     * The residuals moving average
     */
    private double residualMovingAverage = 2.;


    // ----------


    /**
     * Supply all variables required for this local search solver in the constructor.
     *
     * @see NaryCompositionFinderApp for documentation on the constructor arguments.
     */
    public CompositionLocalsearchSolver(
            int dimens,
            int arity,
            Composition compositionImpl,
            Composition metricCompositionImpl,
            NaryStrucons strucons,
            double neighborhoodScale,
            double residualsMovingAverageRate,
            double abortMaxResiduals,
            long minLogTimeInterval
    ) {
        this.dimens = dimens;
        this.arity = arity;
        this.compositionImpl = compositionImpl;
        this.metricCompositionImpl = metricCompositionImpl;
        this.strucons = strucons;
        this.neighborhoodScale = neighborhoodScale;
        this.residualsMovingAverageRate = residualsMovingAverageRate;
        this.abortMaxResiduals = abortMaxResiduals;
        this.minLogTimeInterval = minLogTimeInterval;
    }


    /**
     * Execute the iterative solver algorithm.
     */
    public void solve() {

        logger.info("Solver starting at " + (new Date()));

        Vector[] numbers = new Vector[arity];
        Vector product;
        double productOfComps;
        double compOfProduct;

        iterationCnt = 0;
        IntermittentLogger iLog = new IntermittentLogger(logger, minLogTimeInterval);

        while (!abortIteration) {

            // build a new N-tuple of random N-ary numbers on the unit sphere, apply the composition function,
            // and build the product of these compositions.
            productOfComps = 1.;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                numbers[factorIdx] = new Vector(metricCompositionImpl, dimens).setLength(1.);
                productOfComps *= compositionImpl.calculate(numbers[factorIdx]);
            }

            // calculate the product of these numbers according to the structure constants for N-ary multiplication,
            // apply the composition function, and calculate the residual between the expected and actual composition.
            product = strucons.multiply(numbers);
            compOfProduct = compositionImpl.calculate(product);
            residual = productOfComps - compOfProduct;

            if (logger.isDebugEnabled()) {
                logger.debug("      Product of " + Arrays.toString(numbers));
                logger.debug("               = " + product);
                logger.debug("productOfComps = " + productOfComps);
                logger.debug(" compOfProduct = " + compOfProduct);
                if (logger.isTraceEnabled()) logger.trace("structure constants used:\n" + strucons);
            }

            // If residual is positive, then the product of the numbers' composition function is higher than
            // the composition of the products of the numbers.

            search(strucons, product,
                    compositionImpl,
                    metricCompositionImpl,
                    productOfComps, compOfProduct, residualMovingAverage,
                    neighborhoodScale,
                    numbers);

            // See whether we are stable

            residualMovingAverage = (residualMovingAverage * (1. - residualsMovingAverageRate))
                    + Math.abs(residual) * residualsMovingAverageRate;
            if (residualMovingAverage < abortMaxResiduals) abortIteration = true;

            // Neighborhood search iteration step complete. Advance to the next iteration. Log residual if desired.

            iterationCnt++;

            iLog.log("Iteration=" + iterationCnt
                            + ", residual=" + residual
                            + ", residual moving average=" + residualMovingAverage,
                    iterationCnt);

        }


        logger.info("Look what I found!");
        logger.info("After " + iterationCnt + " iterations the residuals' moving average ("
                + residualMovingAverage + ") is smaller than the abort threshold ("
                + abortMaxResiduals + ").");
        logger.info("This means that the average of residuals from the past "
                + ((int) (1. / residualsMovingAverageRate))
                + " (approx.) iterations were below the threshold.");

        // scrub the structure constants of remnants smaller than the abort condition, then print it:
        logger.info("Resulting structure constants for " + arity + "-ary multiplication in "
                + dimens + " dimensions:\n\n"
                + strucons.scrub(abortMaxResiduals) + "\n");

        logger.info("This " + arity + "-ary multiplication in " + dimens
                + "D satisfies the searched-for composition function ("
                + compositionImpl.getClass().getSimpleName() + ").");
        logger.info("No further constraints were put on searching for the algebra.");
        logger.info("Solver finished at " + (new Date()));

        iterationHasFinished = true;
    }


    // ----------


    public long getIterationCnt() {
        return iterationCnt;
    }

    public boolean getIterationHasFinished() {
        return iterationHasFinished;
    }

    /**
     * If running on a separate thread, call this method to force an iteration abort
     */
    public void abortIteration() {
        this.abortIteration = true;
    }

    public double getMostRecentResidual() {
        return residual;
    }

    public double getResidualMovingAverage() {
        return residualMovingAverage;
    }

}
