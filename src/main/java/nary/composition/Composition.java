/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.composition;

import nary.model.Vector;

public interface Composition {

    /**
     * Build the composition of a single vector.
     */
    double calculate(Vector vector);

    /**
     * Turn the composition of a single vector into a magnitude used for metric calculation.
     * For example, Euclidean and split-Euclidean compositions would use Math.sqrt(abs(length)).
     * <p>
     * Note: This method is intended for normalizing vector lengths according to the metric implied by the
     * composition function. Split-compositions may result in negative values, which are turned into absolute
     * values here. By convention, this function only returns the positive value of the given root.
     */
    double metricOf(double composition, double dimens);

}
