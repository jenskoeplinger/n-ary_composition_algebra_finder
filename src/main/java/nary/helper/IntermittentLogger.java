/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.helper;

import org.slf4j.Logger;

import java.util.Date;

/**
 * Helper class to log "every now and then": Start by logging every k-th step where k follows
 * the Fibunacci series. Once the time between two log entries exceeds minLogTimeInterval [ms],
 * however, end the Fibunacci logic and tune the output to be every minLogTimeInterval on
 * average.
 * <p>
 * If debug-level logging is enabled, log every time.
 * <p>
 * All of this is implemented with minimum overhead during calls where no logging will happen.
 * Only when logging is happening will the condition be determined when the next logging is to
 * be happening. This may cause logging intervals temporarily to be be up to 60% longer than
 * minLogTimeInterval, however, it will eventually regulate on that amount.
 */
public class IntermittentLogger {

    private final Logger logger;

    private final long minLogTimeInterval;

    private long timeAtPrev = new Date().getTime();

    private long logNextStep = 1;

    private long logAtNext = 1;

    private boolean logFibunacci = true;

    public IntermittentLogger(Logger logger, long minLogTimeInterval) {
        this.logger = logger;
        this.minLogTimeInterval = minLogTimeInterval;
    }

    public void log(String message, long count) {
        if (logger.isDebugEnabled() || (count == logAtNext)) {

            logger.info(message);

            long timeNow = new Date().getTime();
            if (((timeNow - timeAtPrev) > minLogTimeInterval) || !logFibunacci) {
                // stop Fibunacci, and tune the step interval towards the desired time interval
                if ((timeNow - timeAtPrev) > minLogTimeInterval) {
                    logNextStep = (9L * logNextStep) / 10L;
                } else {
                    logNextStep = (11L * logNextStep) / 10L;
                }
                logAtNext += logNextStep;
                logFibunacci = false;
            } else {
                // stagger log frequencies Fibunacci, up until minLogTimeInterval is reached
                long newNextStep = logAtNext;
                logAtNext += logNextStep;
                logNextStep = newNextStep;
            }
            timeAtPrev = timeNow;
        }
    }

}
