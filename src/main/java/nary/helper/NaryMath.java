/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.helper;

import nary.composition.Composition;
import nary.composition.impl.CompEuclidean;
import nary.model.NaryStrucons;
import nary.model.Vector;

/**
 * Functions and helpers around N-ary math operations
 */
public class NaryMath {

    /**
     * Performs a structure constants transformation in an 1D subspace (line) from a given transformation
     * matrix, leaving the other (N-1) axes unchanged.
     * <p>
     * Note: This is *not* a rotation of the entries, but a rotation on 1D slices of the structure constants.
     * The slices will be multiplied with the transformationMatrix, and structure constants entries will be added as
     * D-dimensional vectors according to the coefficients of that matrix multiplication.
     *
     * @param factorIdx The factor in the N-ary product represented by the structure constants subject to transformation.
     */
    public static void transformStrucons(NaryStrucons strucons, double[][] transformationMatrix, int factorIdx) {
        int dimens = strucons.getDimens();
        int arity = strucons.getArity();
        Composition metricComposition = strucons.getMetricComposition();
        NaryStrucons resultStrucons = new NaryStrucons(strucons); // deep clone (to leave all hyperspaces unchanged)
        int[] entryCoords = new int[arity];

        for (int entryIdx = 0; entryIdx < strucons.getEntries().length; entryIdx++) {
            // find coordinates of hyperspaces that are indexed independently of the factorIdx

            if (entryCoords[factorIdx] == 0) {
                // subspace found; execute matrix multiplication of structure constants entries

                int[] subspaceCoods = entryCoords.clone();

                for (int factorIdxColumn = 0; factorIdxColumn < dimens; factorIdxColumn++) {
                    Vector newEtry = new Vector(metricComposition, dimens);
                    for (int coefIdx = 0; coefIdx < dimens; coefIdx++) {
                        subspaceCoods[factorIdx] = coefIdx;
                        newEtry.addScaled(strucons.getEntryAt(subspaceCoods), transformationMatrix[factorIdxColumn][coefIdx]);
                    }
                    subspaceCoods[factorIdx] = factorIdxColumn;
                    resultStrucons.setEntryAt(newEtry, subspaceCoods);
                }

            }

            entryCoords = strucons.coordsIncrement(entryCoords);
        }

        strucons.setEntries(resultStrucons.getEntries());
    }


    /**
     * Performs a structure constants transformation in an NxN subspace (plane) from a given transformation
     * matrix, leaving the other (N-2) axes unchanged.
     * <p>
     * Note: This is *not* a rotation of the entries, but a rotation on 2D slices of the structure constants.
     * Only in the 2-ary case are the two equivalent. In the general, N>2 case, the structure constants slices will be
     * multiplied with the transformationMatrix, and structure constants entries will be added as D-dimensional vectors
     * according to the coefficients of that matrix multiplication.
     *
     * @param factorIdx1 The first dimension (factor index) of the structure constants subject to transformation.
     * @param factorIdx2 The second dimension (factor index) of the structure constants subject to transformation.
     */
    public static void transformStrucons(NaryStrucons strucons, double[][] transformationMatrix, int factorIdx1, int factorIdx2) {
        int dimens = strucons.getDimens();
        int arity = strucons.getArity();
        Composition metricComposition = strucons.getMetricComposition();
        NaryStrucons resultStrucons = new NaryStrucons(strucons); // deep clone (to leave all hyperspaces unchanged)
        int[] entryCoords = new int[arity];

        for (int entryIdx = 0; entryIdx < strucons.getEntries().length; entryIdx++) {
            // find coordinates of hyperspaces that are indexed independently of axis1 and axis2

            if ((entryCoords[factorIdx1] == 0) && (entryCoords[factorIdx2] == 0)) {
                // subspace found; execute matrix multiplication of structure constants entries

                int[] subspaceCoods = entryCoords.clone();

                for (int factorIdxRow = 0; factorIdxRow < dimens; factorIdxRow++) {
                    for (int factorIdxColumn = 0; factorIdxColumn < dimens; factorIdxColumn++) {
                        subspaceCoods[factorIdx2] = factorIdxRow;
                        Vector newEntry = new Vector(metricComposition, dimens);
                        for (int coefIdx = 0; coefIdx < dimens; coefIdx++) {
                            subspaceCoods[factorIdx1] = coefIdx;
                            newEntry.addScaled(strucons.getEntryAt(subspaceCoods), transformationMatrix[factorIdxColumn][coefIdx]);
                        }
                        subspaceCoods[factorIdx1] = factorIdxColumn;
                        resultStrucons.setEntryAt(newEntry, subspaceCoods);
                    }
                }

            }

            entryCoords = strucons.coordsIncrement(entryCoords);
        }

        strucons.setEntries(resultStrucons.getEntries());
    }

    /**
     * Creates a perfectly axis-aligned multiplication hypercube that represents the product of four quaternions.
     */
    public static NaryStrucons createQuaternaryQuaternionStrucons() {

        Composition compEuclidean = new CompEuclidean();

        Vector one = new Vector(compEuclidean, 1., 0, 0, 0);
        Vector minusOne = new Vector(one).negate();
        Vector i1 = new Vector(compEuclidean, 0, 1., 0, 0);
        Vector minusI1 = new Vector(i1).negate();
        Vector i2 = new Vector(compEuclidean, 0, 0, 1., 0);
        Vector minusI2 = new Vector(i2).negate();
        Vector i3 = new Vector(compEuclidean, 0, 0, 0, 1.);
        Vector minusI3 = new Vector(i3).negate();

        NaryStrucons quaternionStrucons = new NaryStrucons(compEuclidean, 4, 4); // multiplication arity=4, dimens=4

        // set the first 4x4 plane manually using quaternion multiplication
        quaternionStrucons.setEntryAt(one, 0, 0, 0, 0);
        quaternionStrucons.setEntryAt(i1, 1, 0, 0, 0);
        quaternionStrucons.setEntryAt(i2, 2, 0, 0, 0);
        quaternionStrucons.setEntryAt(i3, 3, 0, 0, 0);
        quaternionStrucons.setEntryAt(i1, 0, 1, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 1, 1, 0, 0);
        quaternionStrucons.setEntryAt(minusI3, 2, 1, 0, 0);
        quaternionStrucons.setEntryAt(i2, 3, 1, 0, 0);
        quaternionStrucons.setEntryAt(i2, 0, 2, 0, 0);
        quaternionStrucons.setEntryAt(i3, 1, 2, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 2, 2, 0, 0);
        quaternionStrucons.setEntryAt(minusI1, 3, 2, 0, 0);
        quaternionStrucons.setEntryAt(i3, 0, 3, 0, 0);
        quaternionStrucons.setEntryAt(minusI2, 1, 3, 0, 0);
        quaternionStrucons.setEntryAt(i1, 2, 3, 0, 0);
        quaternionStrucons.setEntryAt(minusOne, 3, 3, 0, 0);

        Vector entry;
        Vector prod;

        // now calculate all other 4x4 planes by executing this quaternion product

        for (int factorIdx0 = 0; factorIdx0 < 4; factorIdx0++) {
            for (int factorIdx1 = 0; factorIdx1 < 4; factorIdx1++) {

                entry = quaternionStrucons.getEntryAt(factorIdx0, factorIdx1, 0, 0);

                prod = quaternionStrucons.multiply(entry, i1, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 1, 0);
                prod = quaternionStrucons.multiply(entry, i2, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 2, 0);
                prod = quaternionStrucons.multiply(entry, i3, one, one);
                quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, 3, 0);

                for (int factorIdx2 = 0; factorIdx2 < 4; factorIdx2++) {

                    entry = quaternionStrucons.getEntryAt(factorIdx0, factorIdx1, factorIdx2, 0);

                    prod = quaternionStrucons.multiply(entry, i1, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 1);
                    prod = quaternionStrucons.multiply(entry, i2, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 2);
                    prod = quaternionStrucons.multiply(entry, i3, one, one);
                    quaternionStrucons.setEntryAt(prod, factorIdx0, factorIdx1, factorIdx2, 3);

                }
            }
        }

        return quaternionStrucons.scrub(0.1);
    }

    /**
     * Given an N-ary multiplication structure constants in D dimensions and a composition function, executes numTests
     * tests of the composition property. Returns the mean absolute residual from these tests.
     */
    public static double calculateRandomResiduals(NaryStrucons strucons, Composition compositionImpl, int numTests) {
        int dimens = strucons.getDimens();
        int arity = strucons.getArity();
        Composition metricComposition = strucons.getMetricComposition();
        double productOfComps;
        double compOfProduct;
        double residual = 0;

        Vector[] numbers = new Vector[arity];

        for (int resTest = 0; resTest < numTests; resTest++) {

            productOfComps = 1.;
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                numbers[factorIdx] = new Vector(metricComposition, dimens).setLength(1.);
                productOfComps *= compositionImpl.calculate(numbers[factorIdx]);
            }

            // calculate the product of these numbers according to the N-ary structure constants, apply the
            // composition function, and calculate the residual between the expected and actual composition.
            Vector product = strucons.multiply(numbers);
            compOfProduct = compositionImpl.calculate(product);
            residual += Math.abs(productOfComps - compOfProduct);
        }
        return residual / ((double) numTests);
    }

}
