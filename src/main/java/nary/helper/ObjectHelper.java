/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.helper;

/**
 * Sundry basic helpers
 */
public class ObjectHelper {

    public static String getDurationStringFromMillis(long millis) {

        if (millis == Long.MAX_VALUE) {
            return "unknown";
        }
        if (millis == Long.MIN_VALUE) {
            return "not yet started";
        }

        final long SECOND = 1000L;
        final long MINUTE = SECOND * 60L;
        final long HOUR = MINUTE * 60L;
        final long DAY = HOUR * 24L;

        StringBuilder sb = new StringBuilder();

        if (millis < 0) {
            sb.append("negative ");
            millis = -millis;
        }

        long days = millis / DAY;
        millis -= (days * DAY);
        long hours = millis / HOUR;
        millis -= (hours * HOUR);
        long minutes = millis / MINUTE;
        millis -= (minutes * MINUTE);
        long seconds = millis / SECOND;
        millis -= (seconds * SECOND);

        boolean setComma = false;
        int highestHierarchy = 0; // 1=seconds, 2=minutes, 3=hours, 4=days

        if (days > 0) {
            sb.append(days).append(" day").append(pluralS(days));
            setComma = true;
            highestHierarchy = 4;
        }

        if (hours > 0) {
            if (setComma) sb.append(", ");
            sb.append(hours).append(" hour").append(pluralS(hours));
            setComma = true;
            highestHierarchy = 3;
        }

        if ((minutes > 0) && (highestHierarchy < 4)) {
            if (setComma) sb.append(", ");
            sb.append(minutes).append(" minute").append(pluralS(minutes));
            setComma = true;
            highestHierarchy = 2;
        }

        if (highestHierarchy == 2) {
            if (seconds > 0) {
                sb.append(", ");
                sb.append(seconds).append(" second").append(pluralS(seconds));
            }
        } else if (highestHierarchy < 2) {
            if (seconds > 9L) {
                sb.append(String.format("%.1f", ((double) ((seconds * 10L) + (millis / 100L))) / 10.)).append(" seconds");
                setComma = true;
            } else if ((seconds == 1L) && (millis == 0)) {
                sb.append("1 second");
                setComma = true;
            } else if (seconds > 0) {
                sb.append(String.format("%.2f", ((double) ((seconds * 100L) + (millis / 10L))) / 100.)).append(" seconds");
                setComma = true;
            } else if (millis > 0) {
                sb.append(millis).append(" millisecond").append(pluralS(millis));
                setComma = true;
            }
            if (!setComma) {
                sb.append("<1 millisecond");
            }
        }
        return sb.toString();
    }

    public static String pluralS(long number) {
        if (number != 1) return "s";
        else return "";
    }

}

