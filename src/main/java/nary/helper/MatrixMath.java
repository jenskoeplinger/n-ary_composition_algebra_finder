/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary.helper;

import java.util.Random;

/**
 * Sundry helpers around math with matrices.
 */
public class MatrixMath {

    /**
     * Creates a new random SO(2) rotation along one (hyper-)axis of R^D, i.e., in a 2D subspace. These rotations
     * are elements of the DxD matrix representation of SO(D) that leave (D-2) dimensions unchanged.
     *
     * @param random   An instance of Random to allow for determinisitic pseudo-random generation
     * @param dimens   The dimensionality D of R^D
     * @param strength The magnitude of the rotation [0..1], where 1 allows the entire [-pi, pi] interval.
     */
    public static double[][] createRandomSO2Rotation(
            Random random, int dimens, double strength) {

        double[][] rotationMatrix = new double[dimens][dimens];

        // generate a random pair of distinct axis index values
        int axis1 = (int) (random.nextDouble() * ((double) dimens));
        int axis2 = (int) (random.nextDouble() * ((double) (dimens - 1)));
        if (axis2 == axis1) axis2 = dimens - 1;

        // build a NxN rotation matrix
        double angle = Math.PI * strength * (1. - random.nextDouble() * 2.);
        double cosA = Math.cos(angle);
        double sinA = Math.sin(angle);

        for (int axis = 0; axis < dimens; axis++) {
            rotationMatrix[axis][axis] = 1.;
        }
        rotationMatrix[axis1][axis1] = cosA;
        rotationMatrix[axis2][axis2] = cosA;
        rotationMatrix[axis1][axis2] = -sinA;
        rotationMatrix[axis2][axis1] = sinA;

        return rotationMatrix;
    }

    /**
     * Creates a new random O(2) rotation along one (hyper-)axis of R^N, i.e., in a 2D subspace. These rotations
     * are elements of the NxN matrix representation of O(N) that leave (N-2) dimensions unchanged.
     *
     * @param random   An instance of Random to allow for determinisitic pseudo-random generation
     * @param dimens   The dimensionality D of R^D
     * @param strength The magnitude of the rotation [0..1], where 1 allows the entire [-pi, pi] interval. Also
     *                 controls the rate at which an axis flip is probable.
     */
    public static double[][] createRandomO2Rotation(
            Random random, int dimens, double strength) {

        double[][] rotationMatrix = new double[dimens][dimens];

        // generate a random pair of distinct axis index values
        int axis1 = (int) (random.nextDouble() * ((double) dimens));
        int axis2 = (int) (random.nextDouble() * ((double) (dimens - 1)));
        if (axis2 == axis1) axis2 = dimens - 1;

        // build a NxN rotation matrix
        double angle = Math.PI * strength * (1. - random.nextDouble() * 2.);
        double cosA = Math.cos(angle);
        double sinA = Math.sin(angle);

        for (int axis = 0; axis < dimens; axis++) {
            rotationMatrix[axis][axis] = 1.;
        }

        double flip;
        if (random.nextDouble() < strength) {
            flip = -1.;
        } else {
            flip = 1.;
        }

        rotationMatrix[axis1][axis1] = cosA;
        rotationMatrix[axis2][axis2] = cosA * flip;
        rotationMatrix[axis1][axis2] = -sinA * flip;
        rotationMatrix[axis2][axis1] = sinA;

        return rotationMatrix;
    }

    /**
     * Returns the product of an DxD square matrix1 with matrix2, as a new array
     */
    public static double[][] matrixProduct(double[][] matrix1, double[][] matrix2, int dimens) {
        double[][] result = new double[dimens][dimens];
        for (int axisRow = 0; axisRow < dimens; axisRow++) {
            for (int axisColumn = 0; axisColumn < dimens; axisColumn++) {
                double tmp = 0;
                for (int coefIdx = 0; coefIdx < dimens; coefIdx++) {
                    tmp += matrix1[coefIdx][axisRow] * matrix2[axisColumn][coefIdx];
                }
                result[axisRow][axisColumn] = tmp;
            }
        }
        return result;
    }

}
