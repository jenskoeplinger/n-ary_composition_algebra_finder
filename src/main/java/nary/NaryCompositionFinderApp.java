/*
 * CC BY-SA 4.0 ( 2022 ) Jens Koeplinger
 * Creative Commons Attribution-ShareAlike 4.0 International Public License
 * For details, see: https://creativecommons.org/licenses/by-sa/4.0/
 */

package nary;

import nary.composition.Composition;
import nary.composition.CompositionLocalsearchSolver;
import nary.composition.impl.CompEuclidean;
import nary.helper.NaryMath;
import nary.model.NaryStrucons;
import nary.model.Vector;
import nary.simplify.Entropy;
import nary.simplify.NeighborhoodSearch;
import nary.simplify.SimplifyMetaheuristicsSolver;
import nary.simplify.impl.EntropyFavorZeroes;
import nary.simplify.impl.NeighborhoodSearch2DSliceSingleRotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Random;

/**
 * Main entry point: The application that finds structure constants for finite-dimensional algebras over a vector
 * space, which are equipped with an N-ary multiplication that allows for composition of a given form.
 * It attempts to simplify the appearance of the structure constants, to make it more human-friendly and
 * interpretable, by trying to align the basis elements of the algebra with the vector space basis.
 */
public class NaryCompositionFinderApp {

    private final static Logger logger = LoggerFactory.getLogger(NaryCompositionFinderApp.class);


    /**
     * Random seed (if wanted; for reproducibility / deterministic pseudo-random); set to -1 to pick one from time
     */
    public final static long RANDOM_SEED = -1;

    /**
     * Dimensionality of the system.
     */
    public final static int DIMENSION = 4;

    /**
     * The number of factors in the product
     */
    public final static int ARITY = 4;

    /**
     * The composition function to use for satisfying the multiplicative composition property
     */
    public final static Composition COMPOSITION = new CompEuclidean();

    /**
     * The composition function to use for the metric (e.g. defining "unit distance" from the origin). Use for
     * varying algorithm approach only (default: CompEuclidean), will not actually yield an algebraic property.
     */
    public final static Composition METRIC_COMPOSITION = new CompEuclidean();

    /**
     * "Local-search scale" is the amount a structure constant may be shifted
     * during a single iteration if the total residual of the composition
     * function has magnitude 1 (metric composition: Euclidean).
     * <p>
     * Good values:
     * - dimension 2 arity 2 composition Euclidean: 0.01 (complex numbers)
     * - dimension 4 arity 2 composition Euclidean: 0.04 (quaternions)
     * - dimension 4 arity 3 composition Euclidean: 0.05 (ternary quaternions)
     * - dimension 4 arity 3 composition Split:     0.5  (ternary split-quaternions)
     * - dimension 4 arity 4 composition Euclidean: 0.5  (quatquats)
     * - dimension 4 arity 4 composition Split:     0.5  (split-quatquats)
     * - dimension 8 arity 2 composition Euclidean: 0.05 (octonions)
     * - dimension 8 arity 3 composition Euclidean: 0.5  (ternary octonions)
     * - dimension 8 arity 3 composition Split:     0.5  (ternary split-octonions)
     */
    public final static double COMPOSITION_LOCALSEARCH_SCALE = 0.5;

    /**
     * Maximum size of the residuals before considering a solution as "found".
     * Careful when going smaller than significant digits possible in "double" precision
     * (1.e-15), it may not reach the abort condition.
     */
    public final static double COMPOSITION_LOCALSEARCH_ABORT_MAX_RESIDUALS = 2.e-15;

    /**
     * Percentage by which to let the current residual value change the moving average.
     */
    public final static double COMPOSITION_LOCALSEARCH_RESIDUALS_MOVING_AVERAGE_RATE = .01;

    /**
     * The (pseudo-)entropy function to define what is "simple" when looking at the structure constants found.
     * <p>
     * NOTE: This isn't strictly an entropy, I've chosen this term for fancy, to advertise the "lower-is-simpler"
     * goal for a human who is inspecting the structure constants.
     */
    // public final static Entropy SIMPLIFY_ENTROPY = new EntropySortedPairWeightedDistance();
    public final static Entropy SIMPLIFY_ENTROPY = new EntropyFavorZeroes();
    // public final static Entropy SIMPLIFY_ENTROPY = new EntropyAxisAligned();
    // public final static Entropy SIMPLIFY_ENTROPY = new EntropyAxisAlignedSqrt();
    // public final static Entropy SIMPLIFY_ENTROPY = new EntropyAxisAlignedPow2();

    /**
     * The neighborhood search algorithm to use when trying to simplify the structure constants
     */
    public final static NeighborhoodSearch SIMPLIFY_NEIGHBORHOOD_SEARCH = new NeighborhoodSearch2DSliceSingleRotation();
    // public final static NeighborhoodSearch SIMPLIFY_NEIGHBORHOOD_SEARCH = new NeighborhoodSearch1DSliceSingleRotation();
    ///public final static NeighborhoodSearch SIMPLIFY_NEIGHBORHOOD_SEARCH = new NeighborhoodSearch1DSliceSingleOrtho();
    // public final static NeighborhoodSearch SIMPLIFY_NEIGHBORHOOD_SEARCH = new NeighborhoodSearch1DSliceMultiRotation();
    // public final static NeighborhoodSearch SIMPLIFY_NEIGHBORHOOD_SEARCH = new NeighborhoodSearch1DSliceMultiOrtho();

    /**
     * For the "simplify algorithm", size of the ensemble of structure constants to find optimum for
     */
    public final static int SIMPLIFY_STRUCONS_POPULATION_SIZE = 50;

    /**
     * Relative step size for metaheuristic search (tuning factor)
     * (as a hint, you may try increasing for higher dimensionality; decreasing for split-compositions)
     */
    public final static double SIMPLIFY_RELATIVE_STEP_SIZE = 1.;

    /**
     * "Weight factor" to correspond roughly to a number of unimproved iteration steps after which the
     * algorithm should start significantly increasing step size
     */
    public final static double SIMPLIFY_ITERATIONS_UNIMPROVED_STEP_SIZE_ADJUST = 5.;

    /**
     * Entropy value below which the structure constants are to be considered "simplified"
     */
    public final static double SIMPLIFY_ABORT_MAX_ENTROPY = .000001;

    /**
     * When "scrubbing" (rounding) the structure constants for human-friendly output, the rounding precision:
     */
    public final static double STRUCONS_SCRUB_PRECISION = .01;

    /**
     * When to abort simplification after a number of iterations (to give the algorithm
     * a chance to re-set the structure constants, to counteract progressive loss of precision with each iteration
     * that causes the structure constants to increasingly violate the product-composition property of the
     * form, i.e., the original goal of the algorithm from starters)
     */
    public final static long SIMPLIFY_ABORT_MAX_ITERATIONS = 300L;

    /**
     * Minimum time interval in which to log status updates (in regular, non-debug mode)
     */
    public final static long MIN_LOG_TIME_INTERVAL = 1000L; // [ms]


    // ----------


    public static void main(String[] args) {

        Random random;

        logger.info("Start at " + (new Date()));

        if (RANDOM_SEED >= 0) {
            logger.info("Initialize with random seed: " + RANDOM_SEED);
            random = new Random((RANDOM_SEED));
        } else {
            long randomSeed = new Date().getTime();
            logger.info("Initialize with random seed from current time in millis: " + randomSeed);
            random = new Random(randomSeed);
        }
        Vector.setRandom(random);

        logger.info("Create new " + ARITY + "-ary structure constants for an algebra of dimension D=" + DIMENSION
                + ", seeded with random numbers on the unit sphere (S^" + (DIMENSION - 1) + ")");
        logger.info("---- product-composition local-search parameters:");
        logger.info("Composition form to satisfy: " + COMPOSITION.getClass().getSimpleName());
        logger.info("Local-search scale (base): " + COMPOSITION_LOCALSEARCH_SCALE);
        logger.info("Abort max residuals (max difference between composition of product and product of compositions): "
                + COMPOSITION_LOCALSEARCH_ABORT_MAX_RESIDUALS);
        logger.info("Residuals moving average rate (rate of change per new value): "
                + COMPOSITION_LOCALSEARCH_RESIDUALS_MOVING_AVERAGE_RATE
                + " (" + (((int) (COMPOSITION_LOCALSEARCH_RESIDUALS_MOVING_AVERAGE_RATE * 10000.)) / 100.) + "%)");
        logger.info("---- simplification search parameters:");
        logger.info("Metaheuristics algorithm: " + SIMPLIFY_NEIGHBORHOOD_SEARCH.getClass().getSimpleName());

        if (SIMPLIFY_ENTROPY == null) {
            logger.info("Degree of simplicity ('pseudo-entropy') implementation: (varying)");
        } else {
            logger.info("Degree of simplicity ('pseudo-entropy') implementation: "
                    + SIMPLIFY_ENTROPY.getClass().getSimpleName());
        }

        logger.info("Population (ensemble of structure constants) size: "
                + SIMPLIFY_STRUCONS_POPULATION_SIZE);
        logger.info("Abort max entropy (value at which to consider the structure constants 'simplified'): "
                + SIMPLIFY_ABORT_MAX_ENTROPY);
        logger.info("Number of iterations after which to abort the simplification search (if intermittently): "
                + SIMPLIFY_ABORT_MAX_ITERATIONS);

        // Create new random structure constants with entries of length 1 (in arbitrary direction).
        NaryStrucons strucons = new NaryStrucons(METRIC_COMPOSITION, DIMENSION, ARITY).normalize(1.);

        if (logger.isDebugEnabled()) logger.debug("Structure constants created:\n" + strucons);

        boolean finalSolutionFound = false;
        int masterIterationCounter = 0;

        while (!finalSolutionFound) {

            CompositionLocalsearchSolver compositionSolver = new CompositionLocalsearchSolver(
                    DIMENSION,
                    ARITY,
                    COMPOSITION,
                    METRIC_COMPOSITION,
                    strucons,
                    COMPOSITION_LOCALSEARCH_SCALE,
                    COMPOSITION_LOCALSEARCH_RESIDUALS_MOVING_AVERAGE_RATE,
                    COMPOSITION_LOCALSEARCH_ABORT_MAX_RESIDUALS,
                    MIN_LOG_TIME_INTERVAL
            );
            compositionSolver.solve();

            // At this point we have N-ary structure constants, with algebra basis elements in random direction
            // in respect to the vector space basis in R^D. This is incomprehensible to a human.
            //
            // Now: Try to "simplify" (axis-align) the algebra basis elements with the vector space basis, so
            // that the structure constants read more like a conventional multiplication table.

            SIMPLIFY_NEIGHBORHOOD_SEARCH.setRandom(random);

            long abortMaxIterations = SIMPLIFY_ABORT_MAX_ITERATIONS * (1 + masterIterationCounter);

            SimplifyMetaheuristicsSolver simplifySolver = new SimplifyMetaheuristicsSolver(
                    SIMPLIFY_ENTROPY,
                    COMPOSITION,
                    strucons,
                    SIMPLIFY_NEIGHBORHOOD_SEARCH,
                    SIMPLIFY_STRUCONS_POPULATION_SIZE,
                    SIMPLIFY_RELATIVE_STEP_SIZE,
                    SIMPLIFY_ITERATIONS_UNIMPROVED_STEP_SIZE_ADJUST,
                    SIMPLIFY_ABORT_MAX_ENTROPY,
                    STRUCONS_SCRUB_PRECISION,
                    abortMaxIterations,
                    MIN_LOG_TIME_INTERVAL
            );
            simplifySolver.solve();

            // The simplification algorithm returned, but may not have found a solution yet. As a result of compound
            // rounding precision errors from the simplification iterations, the structure constants may not
            // represent an algebra anymore, with the required accuracy, that preserves product-composition of
            // the given form. Therefore, re-run the composition solver to ensure we're not drifting away from the
            // overall objective.

            finalSolutionFound = simplifySolver.wasBestEntropyFound();
            masterIterationCounter++;
        }

        logger.info("Solution found.");

        testPairwiseComposition(
                strucons,
                COMPOSITION,
                METRIC_COMPOSITION,
                10000,
                STRUCONS_SCRUB_PRECISION * 10.,
                random);

    }

    // ----------


    /**
     * Given structure constants of an algebra that satisfies product-composition property of a given form under
     * N-ary multiplication, test whether pairwise (binary, 2-ary) products in that algebra (after fixing the remaining
     * factors as constants) also satisfy that composition.
     * <p>
     * For example, ternary split-quaternions have a nondegenerate trilinear form, however, don't have a
     * binary-product counterpart. The reason for this is that the algebra is not unital. Nonunital algebras
     * generally don't allow to be recast into algebras of lower arity. Therefore, this test here will fail on
     * (some of) the binary products in the ternary split-quaternions.
     * <p>
     * Returns the number of tests that failed the composition test.
     * <p>
     * TODO fix the following flaw
     * NOTE: This method is flawed, since dimensions that are assumed constant contribute to the composition
     * also, depending on what `COMPOSITION` is set to. For example, if the split-quadratic form is searched
     * for, the constant basis elements may contribute a factor +1 of -1 to the composition. Therefore, this
     * test here may fail half of the tests, where in fact the product-composition property holds throughout.
     * Example: Arity 3, dimension 4, split-composition.
     */
    private static int testPairwiseComposition(
            NaryStrucons strucons,
            Composition compositionImpl,
            Composition metricComposition,
            int numTests,
            double residualThreshold,
            Random random) {

        int dimens = strucons.getDimens();
        int arity = strucons.getArity();

        logger.info("Testing for pairwise composition property of " + dimens + "D numbers from the "
                + arity + "-ary product");

        double residualTest = NaryMath.calculateRandomResiduals(strucons, compositionImpl, 100);
        if (residualTest > residualThreshold) {
            logger.warn("The algebra itself may not have composition property (residual test "
                    + residualTest + "); continuing anyway ...");
        }

        if (arity < 2) {
            logger.info("(trivially satisfied for " + arity + "-ary product)");
            return 0; // "all success"
        }

        int numFailed = 0;

        for (int test = 0; test < numTests; test++) {

            Vector factor1 = new Vector(metricComposition, dimens).setLength(1.);
            Vector factor2 = new Vector(metricComposition, dimens).setLength(1.);

            // (1) product of the vectors' compositions
            double productOfComps = compositionImpl.calculate(factor1) * compositionImpl.calculate(factor2);

            // (2) composition of the (sub)product

            // pick the factor indices
            int factorIdx1 = (int) (random.nextDouble() * ((double) (arity - 1)));
            int factorIdx2 = factorIdx1 + 1;

            // determine the invariant slice to be tested
            StringBuilder invS = new StringBuilder();
            invS.append("[");
            int[] fixedEntryCoords = new int[arity];
            for (int factorIdx = 0; factorIdx < arity; factorIdx++) {
                if (factorIdx > 0) invS.append(",");
                if ((factorIdx != factorIdx1) && (factorIdx != factorIdx2)) {
                    fixedEntryCoords[factorIdx] = (int) (Math.random() * dimens);
                    invS.append(fixedEntryCoords[factorIdx]);
                } else {
                    fixedEntryCoords[factorIdx] = -1; // should never be queried
                    invS.append("_");
                }
            }
            invS.append("]");
            Vector product = strucons.multiplyBinary(factor1, factorIdx1, factor2, factorIdx2, fixedEntryCoords);
            double compOfProduct = compositionImpl.calculate(product);


            // (3) check residual
            String prodString = "sub-product for factors (" + factorIdx1
                    + "," + factorIdx2 +
                    "), invariant slice " + invS
                    + "; product: "
                    + factor1 + " * " + factor2 + " = " + product;

            double residual = Math.abs(compOfProduct - productOfComps);
            if (residual > residualThreshold) {
                prodString = "FAILED (" + residual + " > " + residualThreshold + "): " + prodString;
                numFailed++;
            } else {
                if (residual <= 1.e-15) residual = 0;
                prodString = "PASS (res=" + residual + "): " + prodString;
            }
            if ((test < 50) || ((numTests - test) < 50)) {
                logger.info(prodString);
            } else if ((test == 50) && (numTests > 100)) {
                logger.info("   ... (" + (numTests - 100) + " more)");
            }

        }

        logger.info("Total number of failed pairwise composition: " + numFailed + " (out of " + numTests + ")");

        return numFailed;

    }


}
